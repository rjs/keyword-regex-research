/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef PM_AHOCORASICKMATCHER_HPP
#define PM_AHOCORASICKMATCHER_HPP

/*
 * Standard Aho-Corasick pattern-matching automaton. Matches all
 * keywords in the given patterns, completely ignoring gaps.
 */

#include "Matcher.hpp"
#include "TransitionTable.hpp"
#include "util-Log.hpp"
#include "util-BitSet.hpp"
#include <map>
#include <list>
#include <utility>
#include <vector>

class AhoCorasickMatcher: public Matcher {
public:
  typedef std::pair<unsigned int, unsigned int> MatchPosType;

  class OutputTuple: public MatchPosType {

  public:
    OutputTuple() :
      MatchPosType(0, 0) {
    }
    OutputTuple(unsigned int pattern, unsigned int keyword) :
      MatchPosType(pattern, keyword) {
    }

    unsigned int pattern() const {
      return first;
    }

    unsigned int keyword() const {
      return second;
    }
  };

  AhoCorasickMatcher(std::vector<Pattern> & patterns);
  virtual ~AhoCorasickMatcher();

  virtual void findMatches(Reader *src_);

private:
  void traverseOutputPath(unsigned int q, unsigned int charCount);

  void countMaxDistAndKWLen();
  void addKeyword(std::string const & keyword,
                  std::vector<std::map<chartype,unsigned int> > & pgotos,
                  unsigned int patId, unsigned int kwId);
  void constructFailStates(
    std::vector<std::map<chartype,unsigned int> > const & pgotos);
  void addFailsToGotos(std::vector<std::map<chartype,unsigned int> > & pgotos);
  void addFailsToOutputs();

  unsigned int newState;
  unsigned int nStates;
  util::BitSet keywordState;
  std::vector<std::vector<OutputTuple> > outputs;
  TransitionTable<chartype, unsigned int> *gotos;
  std::set<chartype> usedChars;
  std::vector<unsigned int> fails;
};

class AhoCorasickMatcherFactory: public MatcherFactory {
public:
  AhoCorasickMatcherFactory() { }

  Matcher * createMatcher(std::vector<Pattern> & patterns) {
    return new AhoCorasickMatcher(patterns);
  }
};

#endif // PM_AHOCORASICKMATCHER_HPP
