/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Provides a timer that calculates time from its initialization and
 * last call to it
 */

/*
 * There is some Windows-specific code here, behind the WIN32 ifdefs,
 * but we have not tested these algorithms on Windows: probably more
 * porting is required than what is here.
 */

#include "util-Timer.hpp"
#include "util-Log.hpp"

#include <cmath>

using namespace util;

#if !defined(WIN32)

namespace util {
  inline double tv_diff_secs(struct timeval * start, struct timeval * end) {
    time_t secs = end->tv_sec - start->tv_sec;
    // end must be >= start
    ASSERT(secs >= 0);
    if (secs == 0) {
      ASSERT(end->tv_usec >= start->tv_usec);
      // usec = microseconds
      return (end->tv_usec - start->tv_usec) / (double) 1000000.0;
    }
    return (secs - 1) + // The whole seconds without the first second
      (((1000000 - start->tv_usec) + // the leftovers from first second
        end->tv_usec) // the last second's leftovers
       / (double) 1000000.0); // .. microseconds scaled to seconds
  }
}

#endif // !defined(WIN32)

Timer::Timer() :
  paused(true), accumulated(0) {
  start();
  paused = true;
}

Timer::~Timer() {
}

/*
 * (Re-)starts the timer
 */
void Timer::start() {
#if defined(WIN32)
  startTime = timeGetTime();
  lastTime = startTime;
#else // defined(WIN32)
  gettimeofday(&startTime, 0);
  lastTime = startTime;
#endif // defined(WIN32)
  paused = false;
}

/*
 * Pauses the timer
 */
void Timer::pause() {
  if (paused)
    return;
  accumulated = timeFromStart();
  paused = true;
}

/*
 * Returns the time from the start of the timer without including pauses.
 */
double Timer::timeFromStart() {
  if (paused) {
    return accumulated;
  }

#if defined(WIN32)
  return accumulated + fabs((double)(timeGetTime() - startTime)
                            / (double)1000.0);
#else // defined(WIN32)
  struct timeval current;
  gettimeofday(&current, 0);
  return accumulated + tv_diff_secs(&startTime, &current);
#endif // defined(WIN32)
}

/*
 * Returns the time from the last (absolute) invocation of this method.
 * Pauses are not handled so they do not affect the outcome of this
 * method.
 */
double Timer::timeFromLast() {
#if defined(WIN32)
  // Windows
  DWORD current = timeGetTime();
  DWORD diff = current - lastTime;
  lastTime = current;
  return fabs((double)diff / (double)1000.0);
#else // defined(WIN32)
  struct timeval current;
  gettimeofday(&current, 0);
  double diff = tv_diff_secs(&lastTime, &current);
  lastTime = current;
  return diff;

#endif // defined(WIN32)
}
