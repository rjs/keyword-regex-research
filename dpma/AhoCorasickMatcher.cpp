/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Standard Aho-Corasick pattern-matching automaton. Matches all
 * keywords in the given patterns, completely ignoring gaps.
 */

#include "AhoCorasickMatcher.hpp"
#include "Reader.hpp"
#include "Utils.hpp"
#include "util-Log.hpp"
#include "util-Collections.hpp"
#include <cstring>
#include <queue>
#include <algorithm>

using namespace std;

#define INITIAL_STATE 0
#define NONEXISTENT_STATE (static_cast<unsigned int>(-1))

/*
 * This optional optimization adds fail transitions directly to the
 * transition tables of each state, avoiding the need to traverse the
 * fail chain during matching. Uncomment the define to disable it.
 */
#define FAILS_TO_GOTOS

AhoCorasickMatcher::AhoCorasickMatcher(std::vector<Pattern> & patterns) :
  Matcher(patterns), newState(INITIAL_STATE), nStates(0) {

  vector<map<chartype, unsigned int> > pgotos;
  pgotos.push_back(map<chartype, unsigned int> ());
  for (unsigned int patId = 0; patId < patterns.size(); patId++) {
    Pattern const & pattern = patterns[patId];
    for (unsigned int kwId = 0; kwId < pattern.getNumKeywords(); kwId++) {
      addKeyword(pattern.getKeyword(kwId), pgotos, patId, kwId);
    }
  }
  nStates = newState + 1;
  ASSERTEQUALS((size_t) nStates, pgotos.size());

  fails.resize(nStates);
  outputs.resize(nStates);

  constructFailStates(pgotos);

#ifdef FAILS_TO_GOTOS
  addFailsToGotos(pgotos);
#endif
  addFailsToOutputs();

  gotos = new TransitionTable<chartype, unsigned int> [nStates];
  for (size_t i = 0; i < nStates; i++) {
#ifdef FAILS_TO_GOTOS
    gotos[i].set_all(pgotos[i], INITIAL_STATE);
#else
    gotos[i].set_all(pgotos[i],
                     i == INITIAL_STATE
                     ? INITIAL_STATE : NONEXISTENT_STATE);
#endif
  }

  ASSERTEQUALS((unsigned int) newState + 1, nStates);
}

AhoCorasickMatcher::~AhoCorasickMatcher() {
  delete[] gotos;
}

void AhoCorasickMatcher::addKeyword(std::string const & keyword,
                                    vector<map<chartype, unsigned int> > & pgotos,
                                    unsigned int patId, unsigned int kwId) {
  if (keyword == "") {
    // Ignore any empty keywords
    return;
  }
  unsigned int state = 0;
  unsigned int j = 0;
  map<chartype, unsigned int>::const_iterator existing;
  while (j < keyword.length() &&
         (existing = pgotos[state].find(keyword[j])) != pgotos[state].cend()) {
    state = (*existing).second;
    j++;
  }
  for (unsigned int p = j; p < keyword.length(); p++) {
    // If there are multiple identical keywords, then this loop might
    // not be run
    newState++;
    pgotos.push_back(map<chartype, unsigned int> ());
    pgotos[state][keyword[p]] = newState;
    usedChars.insert(keyword[p]);
    state = newState;
    ASSERTEQUALS((size_t) newState + 1, pgotos.size());
  }
  if (!keywordState[state]) {
    // Mark state as a keyword state
    Matcher::stats.logOperation(Statistics::KeywordState);
    keywordState[state] = true;
  }
  if (outputs.size() < state + 1)
    outputs.resize(state + 1);
  outputs[state].push_back(OutputTuple(patId, kwId));
}

void AhoCorasickMatcher::constructFailStates(
  vector<map<chartype,unsigned int> > const & pgotos) {
  queue<unsigned int> queue;
  fails[INITIAL_STATE] = INITIAL_STATE;
  for (set<chartype>::const_iterator it = usedChars.cbegin();
       it != usedChars.cend();
       ++it) {
    chartype a = *it;
    if (pgotos[INITIAL_STATE].find(a) != pgotos[INITIAL_STATE].cend()) {
      unsigned int s = pgotos[INITIAL_STATE].find(a)->second;
      queue.push(s);
      fails[s] = INITIAL_STATE;
    }
  }
  while (!queue.empty()) {
    unsigned int r = queue.front();
    queue.pop();
    for (set<chartype>::const_iterator it = usedChars.cbegin();
         it != usedChars.cend();
         ++it) {
      chartype a = *it;
      if (pgotos[r].find(a) != pgotos[r].cend()) {
        unsigned int s = pgotos[r].find(a)->second;
        queue.push(s);
        unsigned int state = fails[r];
        while (state != INITIAL_STATE &&
               pgotos[state].find(a) == pgotos[state].cend()) {
          state = fails[state];
        }
        if (pgotos[state].find(a) == pgotos[state].cend())
          fails[s] = INITIAL_STATE;
        else
          fails[s] = pgotos[state].find(a)->second;
      }
    }
  }
}

void AhoCorasickMatcher::addFailsToGotos(
  vector<map<chartype,unsigned int> > & pgotos) {
  for (size_t i = 0; i < nStates; i++) {
    size_t s = i;
    do {
      s = fails[s];
      for (auto p : pgotos[s]) {
        chartype c = p.first;
        unsigned int v = p.second;
        if (pgotos[i].find(c) == pgotos[i].cend()) {
          pgotos[i][c] = v;
        }
      }
    } while (s != INITIAL_STATE);
  }
}

void AhoCorasickMatcher::addFailsToOutputs() {
  // FIXME Could be optimized: traverses the fail chains multiple
  // times and find is slow (use a set?)
  for (size_t i = 0; i < nStates; i++) {
    size_t s = i;
    do {
      s = fails[s];
      for (OutputTuple o : outputs[s]) {
        if (find(outputs[i].cbegin(), outputs[i].cend(), o) == outputs[i].cend())
          outputs[i].push_back(o);
      }
    } while (s != INITIAL_STATE);
  }
}

void AhoCorasickMatcher::findMatches(Reader *src_) {
  src = src_;

  unsigned int state = INITIAL_STATE;
  chartype nextChar;

  charCount = 0;

  try {
    for (;;) {
      nextChar = static_cast<chartype>(src->readChar());
      LOG("Read " << static_cast<int>(nextChar) << " (" << nextChar << ")");
      // stats.logOperation(Statistics::ProcessChar);
      ASSERT(nextChar < 256);
      ASSERT(nextChar >= 0);
      charCount++;
      if (nextChar == '\n')
        lineBreak();

#ifdef FAILS_TO_GOTOS
      state = gotos[state].get(nextChar);
#else
      unsigned int s;
      while ((s = gotos[state].get(nextChar)) == NONEXISTENT_STATE)
        state = fails[state];
      state = s;
#endif

      traverseOutputPath(state, charCount);
    }
  } catch (Reader::end_of_data &e) {
    ;
  } catch (stop_matching &e) {
    ;
  }

  src = nullptr;
}

void AhoCorasickMatcher::traverseOutputPath(unsigned int q,
                                            unsigned int charCount) {
  // Do something for this keyword (state q)
  // Process keywords that occur at this state
  for (OutputTuple o : outputs[q]) {
    stats.logOperation(Statistics::KeywordMatch);
    reportMatch(charCount - 1, o.pattern());
  }
}
