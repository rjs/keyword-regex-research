/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Parser for POSIX extended regular expressions (together with the
 * Pattern class which is used to store the result of parsing).
 */

#ifndef PM_PATTERNPARSER_HPP
#define PM_PATTERNPARSER_HPP

#include "Pattern.hpp"
#include "Utils.hpp"
#include "util-Collections.hpp"
#include <memory>
#include <stack>
#include <string>
#include <set>
#include <iostream>
#include <algorithm>

class PtKeyword;
class PtBoundary;
class PtElement: public virtual Printable {
  friend class PatternParser;
public:
  PtElement() { }
  virtual ~PtElement() { }
  void print(std::ostream & os) const {
    os << "?element?";
  }

  virtual bool mustMatchKeyword() const { return false; }
  // Use p to allocate keyword ids into PtKeyword::kwId
  virtual void findKeywords(Pattern & p) { }
  // Calculate PtKeyword::following_kws
  virtual void calcFollowingKws(std::vector<PtKeyword*> & preceding_kws) { }
  // Set followsets calculated by calcFollowingKws into p
  virtual void setFollowSets(Pattern & p) { }
  // Calculate PtKeyword::preBoundary and set it into p
  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  // Mirror image of calcPreBoundaries (calculate PtKeyword::postBoundary)
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);
  // Calculate gaps
  virtual void calcGaps(Gap & gap, Pattern & p) const { }
  // All strings that can match the pattern
  virtual void getMatchingStrings(std::set<std::string> & s) const { }
};

class PtEmpty: public PtElement {
  friend class PatternParser;
public:
  PtEmpty() : PtElement() { }
  virtual ~PtEmpty() { }

  void print(std::ostream & os) const {
    os << "nil";
  }

  void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) { }
  void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) { }

  void getMatchingStrings(std::set<std::string> & s) const {
    s.insert("");
  }
};

class PtKeyword: public PtElement {
  friend class PatternParser;
public:
  explicit PtKeyword(std::string const & str)
    : PtElement(), kwId(0), following_kws(), keyword(str),
      preBoundary(0), postBoundary(0) { }
  virtual ~PtKeyword() { }

  void print(std::ostream & os) const;
  std::string const & getKeyword() const {
    return keyword;
  }
  keywordid_t getKeywordId() const {
    return kwId;
  }

  virtual bool mustMatchKeyword() const { return true; }

  virtual void findKeywords(Pattern & p) {
    kwId = p.addKeyword(keyword);
  }

  virtual void calcFollowingKws(std::vector<PtKeyword*> & preceding_kws) {
    for (PtKeyword *k : preceding_kws)
      k->following_kws.push_back(this);
    preceding_kws.clear();
    preceding_kws.push_back(this);
  }

  virtual void setFollowSets(Pattern & p) {
    std::vector<keywordid_t> ids;
    for (PtKeyword *k : following_kws)
      ids.push_back(k->kwId);
    p.setFollowSet(kwId, ids);
    LOG("Follow set for keyword #" << kwId << "=" << keyword << ": " << ids);
  }

  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    gap.finishAdding();
    p.setGap(kwId, gap);
    LOG("Gap for keyword #" << kwId << "=" << keyword << ": " << gap);
    gap = Gap();
  }

  void getMatchingStrings(std::set<std::string> & s) const {
    s.insert(keyword);
  }

protected:
  keywordid_t kwId;
  std::vector<PtKeyword*> following_kws;
private:
  std::string keyword;
  PtBoundary *preBoundary, *postBoundary;
};

class PtKeywordSentinel: public PtKeyword {
  friend class PatternParser;
public:
  explicit PtKeywordSentinel(keywordid_t kwId_)
    : PtKeyword("") {
    kwId = kwId_;
  }
  virtual ~PtKeywordSentinel() { }

  virtual void findKeywords(Pattern & p) { }
  virtual void setFollowSets(Pattern & p) {
    if (kwId == Pattern::begin_keyword_id) {
      std::vector<keywordid_t> ids;
      for (PtKeyword *k : following_kws)
        if (k->getKeywordId() != Pattern::end_keyword_id)
          ids.push_back(k->getKeywordId());
        else
          LOG("Begin set of pattern id#" << p.getId()
              << " contains end keyword!");
      p.setBeginSet(ids);
      LOG("Begin set: " << ids);
    }
  }

  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    if (kwId == Pattern::begin_keyword_id) {
      // Add an initial gap .* (does not need fallback)
      // FIXME keywords starting with ^ could be optimized by
      // disabling them if they don't match at the initial position
      gap = Gap();
      gap.addAtEnd(0, Gap::infinite, nullptr);
    }
    // FIXME Do something else with final gaps?
    if (kwId == Pattern::end_keyword_id
        && (gap.anythingAdded() || gap.needsFallback())) {
      gap.finishAdding();
      p.establishPostFallback(gap.needsFallback()
                              ? gap.getMaxLength() : gap.getMinLength());
    }
  }

  void getMatchingStrings(std::set<std::string> & s) const {
    s.insert("");
  }
};

class PtGap: public PtElement {
  friend class PatternParser;
public:
  explicit PtGap(gap_size_t minLength_,
                 gap_size_t maxLength_,
                 const CharClass *cc_)
    : PtElement(), minLength(minLength_), maxLength(maxLength_), cc(cc_) { }
  virtual ~PtGap() { }
  void print(std::ostream & os) const {
    os << "(gap " << minLength << " " << maxLength << " ";
    if (cc)
      os << cc;
    else
      os << ".";
    os << ")";
  }

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    gap.addAtEnd(minLength, maxLength, cc);
  }

  void getMatchingStrings(std::set<std::string> & s) const {
    std::string es;
    if (cc) {
      cc->matching_chars(es);
    } else {
      CharClass::dotCC->matching_chars(es);
    }
    size_t e = es.size();

    ASSERT(maxLength != Gap::infinite);

    if (minLength == 0)
      s.insert("");

    if (minLength == 1) {
      for (auto & c : es) {
        std::string tmps(1, c);
        s.insert(tmps);
      }
    }

    std::vector<size_t> e_to(maxLength + 1);
    e_to[0] = 1;
    for (size_t i = 1; i <= maxLength; ++i)
      e_to[i] = e_to[i - 1] * e;

    size_t r = (minLength <= 1) ? 2 : minLength;
    for (; r <= maxLength; ++r) {
      for (size_t c = 0; c < e_to[r]; ++c) {
        std::ostringstream ss;
        for (size_t i = 0; i < r; ++i)
          ss << es[c / e_to[i] % e];
        s.insert(ss.str());
      }
    }
  }

private:
  gap_size_t minLength, maxLength;
  const CharClass *cc;
};

class PtOr: public PtElement {
  friend class PatternParser;
public:
  PtOr() : PtElement(), choices() { }
  explicit PtOr(std::shared_ptr<PtElement> firstChoice)
    : PtOr() {
    addChoice(firstChoice);
  }
  virtual ~PtOr() { }

  void print(std::ostream & os) const {
    os << "(or";
    for (auto & c : choices)
      os << " " << *c;
    os << ")";
  }

  size_t nChoices() const {
    return choices.size();
  }
  std::shared_ptr<PtElement> getChoice(size_t i) const {
    return choices[i];
  }
  void addChoice(std::shared_ptr<PtElement> choice) {
    if (!choice)
      choice = std::make_shared<PtEmpty>();
    choices.push_back(choice);
  }

  virtual bool mustMatchKeyword() const {
    for (auto & c : choices)
      if (!c->mustMatchKeyword())
        return false;
    return true;
  }

  virtual void findKeywords(Pattern & p) {
    for (auto & c : choices)
      c->findKeywords(p);
  }

  virtual void calcFollowingKws(std::vector<PtKeyword*> & preceding_kws) {
    std::vector<std::vector<PtKeyword*> > newprec(choices.size(),
                                                  preceding_kws);
    for (size_t i = 0; i != choices.size(); ++i)
      choices[i]->calcFollowingKws(newprec[i]);

    // Merge everything from newprec[] into preceding_kws
    preceding_kws.clear();
    for (std::vector<PtKeyword*> & ks : newprec)
      for (PtKeyword* k : ks)
        if (std::find(preceding_kws.cbegin(), preceding_kws.cend(), k)
            == preceding_kws.cend())
          preceding_kws.push_back(k);
  }

  virtual void setFollowSets(Pattern & p) {
    for (auto & c : choices)
      c->setFollowSets(p);
  }

  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    std::vector<Gap> newgaps(choices.size(), gap);
    for (size_t i = 0; i != choices.size(); ++i)
      choices[i]->calcGaps(newgaps[i], p);

    // FIXME could sometimes merge part of newgaps[] into gap
    bool allempty = true, needfb = false;
    for (size_t i = 0; i != choices.size(); ++i) {
      if (newgaps[i].anythingAdded())
        allempty = false;
      if (newgaps[i].needsFallback())
        needfb = true;
    }
    if (allempty) {
      gap = Gap();
      if (needfb)
        gap.setNeedFallback();
    } else {
      gap = Gap(0, Gap::infinite);
    }
  }

  void getMatchingStrings(std::set<std::string> & s) const {
    for (auto & c : choices) {
      c->getMatchingStrings(s);
    }
  }

private:
  std::vector<std::shared_ptr<PtElement> > choices;
};

class PtRepeating: public PtElement {
  friend class PatternParser;
public:
  PtRepeating(std::shared_ptr<PtElement> elt_, gap_size_t minr_, gap_size_t maxr_)
    : PtElement(), elt(elt_), minr(minr_), maxr(maxr_) {
    ASSERT(minr <= maxr);
    ASSERT(maxr > 0);
    ASSERT(!!elt);
  }
  virtual ~PtRepeating() { }

  void print(std::ostream & os) const {
    os << "(rep " << minr << " ";
    if (maxr == Gap::infinite)
      os << "inf";
    else
      os << maxr;
    os << " " << *elt << ")";
  }

  virtual bool mustMatchKeyword() const {
    return (minr != 0) && elt->mustMatchKeyword();
  }

  virtual void findKeywords(Pattern & p) { elt->findKeywords(p); }

  virtual void calcFollowingKws(std::vector<PtKeyword*> & preceding_kws) {
    if (minr == 0) {
      std::vector<PtKeyword*> oldprec(preceding_kws);
      elt->calcFollowingKws(preceding_kws);
      // Merge everything from oldprec into preceding_kws
      for (PtKeyword* k : oldprec)
        if (std::find(preceding_kws.cbegin(), preceding_kws.cend(), k)
            == preceding_kws.cend())
          preceding_kws.push_back(k);
    } else {
      elt->calcFollowingKws(preceding_kws);
    }
  }

  virtual void setFollowSets(Pattern & p) { elt->setFollowSets(p); }

  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    bool origgapnonempty = gap.anythingAdded();
    bool origgapfb = gap.needsFallback();
    if (origgapnonempty && maxr > 1)
      gap = Gap(0, Gap::infinite);
    // If maxr==1, keep the gap as is (it can be used in matching elt)
    // else keep the empty (possibly fallback) gap
    elt->calcGaps(gap, p);
    if (gap.anythingAdded() || (minr == 0 && origgapnonempty))
      gap = Gap(0, Gap::infinite);
    else if (origgapfb)
      // keep the empty gap but set fallback
      gap.setNeedFallback();
    // else keep the empty (possibly fallback) gap
  }

  void getMatchingStrings(std::set<std::string> & s) const {
    std::set<std::string> esset;
    elt->getMatchingStrings(esset);
    std::vector<std::string> es(esset.cbegin(), esset.cend());
    size_t e = es.size();

    ASSERT(maxr != Gap::infinite);

    if (minr == 0)
      s.insert("");

    if (minr <= 1)
      s.insert(es.cbegin(), es.cend());

    std::vector<size_t> e_to(maxr + 1);
    e_to[0] = 1;
    for (size_t i = 1; i <= maxr; ++i)
      e_to[i] = e_to[i - 1] * e;

    size_t r = (minr <= 1) ? 2 : minr;
    for (; r <= maxr; ++r) {
      for (size_t c = 0; c < e_to[r]; ++c) {
        std::ostringstream ss;
        for (size_t i = 0; i < r; ++i)
          ss << es[c / e_to[i] % e];
        s.insert(ss.str());
      }
    }
  }

private:
  std::shared_ptr<PtElement> elt;
  gap_size_t minr, maxr;
};

class PtBoundary: public PtElement {
  friend class PatternParser;
public:
  explicit PtBoundary(char btype_)
    : PtElement(), btype(btype_),
      notFullyProcessedPre(false), notFullyProcessedPost(false) { }
  virtual ~PtBoundary() { }

  void print(std::ostream & os) const {
    os << "(b"
       << (isNotFullyProcessed() ? " " : "F ")
       << btype << ")";
  }

  char getType() const {
    return btype;
  }

  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    // Ignore fully processed gaps (they are already part a of a keyword)
    if (isNotFullyProcessed())
      gap.setNeedFallback();
  }

  void setNotFullyProcessedPre() {
    notFullyProcessedPre = true;
  }
  void setNotFullyProcessedPost() {
    notFullyProcessedPost = true;
  }
  bool isNotFullyProcessed() const {
    return notFullyProcessedPre && notFullyProcessedPost;
  }

private:
  char btype;
  bool notFullyProcessedPre, notFullyProcessedPost;
};

class PtSubExpr: public PtElement {
  friend class PatternParser;
public:
  PtSubExpr(std::shared_ptr<PtElement> elt_, unsigned int n_)
    : PtElement(), elt(elt_), n(n_) {
    if (!elt)
      elt = std::make_shared<PtEmpty>();
  }
  virtual ~PtSubExpr() { }

  void print(std::ostream & os) const {
    os << "(s " << n << " " << *elt << ")";
  }

  virtual bool mustMatchKeyword() const { return elt->mustMatchKeyword(); }
  virtual void findKeywords(Pattern & p) { elt->findKeywords(p); }
  virtual void calcFollowingKws(std::vector<PtKeyword*> & preceding_kws) {
    elt->calcFollowingKws(preceding_kws);
  }
  virtual void setFollowSets(Pattern & p) { elt->setFollowSets(p); }
  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
    elt->calcPreBoundaries(immpre_b, p);
  }
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
    elt->calcPostBoundaries(immpost_b, p);
  }
  virtual void calcGaps(Gap & gap, Pattern & p) const {
    elt->calcGaps(gap, p);
  }
  void getMatchingStrings(std::set<std::string> & s) const {
    elt->getMatchingStrings(s);
  }

private:
  std::shared_ptr<PtElement> elt;
  unsigned int n;
};

class PtBackreference: public PtElement {
  friend class PatternParser;
public:
  explicit PtBackreference(unsigned int n_) : PtElement(), n(n_) { }
  virtual ~PtBackreference() { }

  // FIXME PtOr also creates a numbered subexpression (number not stored)
  // FIXME and then simplifyMakekws may create more numbered subexpressions

  void print(std::ostream & os) const {
    os << "(bref " << n << ")";
  }

  char getN() const {
    return n;
  }

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    // Let the fallback matcher handle all backreference matching
    // FIXME could sometimes calculate min/max lengths from the subexpression
    gap = Gap(0, Gap::infinite);
  }

private:
  unsigned int n;
};

class PtConcat: public PtElement {
  friend class PatternParser;
public:
  PtConcat() : PtElement(), elements() { }
  explicit PtConcat(std::shared_ptr<PtElement> first)
    : PtConcat() {
    addToEnd(first);
  }
  virtual ~PtConcat() { }

  void print(std::ostream & os) const {
    os << "(c";
    for (auto & c : elements)
      os << " " << *c;
    os << ")";
  }

  size_t nElements() const {
    return elements.size();
  }
  std::shared_ptr<PtElement> getElement(size_t i) const {
    return elements[i];
  }
  void addToEnd(std::shared_ptr<PtElement> elt) {
    ASSERT(!!elt);
    elements.push_back(elt);
  }
  void replace(size_t i, std::shared_ptr<PtElement> elt) {
    ASSERT(!!elt);
    elements[i] = elt;
  }

  virtual bool mustMatchKeyword() const {
    for (auto & e : elements)
      if (e->mustMatchKeyword())
        return true;
    return false;
  }

  virtual void findKeywords(Pattern & p) {
    for (auto & e : elements)
      e->findKeywords(p);
  }

  virtual void calcFollowingKws(std::vector<PtKeyword*> & preceding_kws) {
    for (auto & e : elements)
      e->calcFollowingKws(preceding_kws);
  }

  virtual void setFollowSets(Pattern & p) {
    for (auto & e : elements)
      e->setFollowSets(p);
  }

  virtual void calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p);
  virtual void calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p);

  virtual void calcGaps(Gap & gap, Pattern & p) const {
    for (auto & e : elements)
      e->calcGaps(gap, p);
  }

  // Matching strings from elements[a] .. elements[b] inclusive
  void getMatchingStrings(std::set<std::string> & s,
                          size_t a, size_t b) const {
    std::set<std::string> s1, s2;
    std::set<std::string> *os = &s1, *ns = &s2;
    elements[a]->getMatchingStrings(*os);
    for (size_t i = a + 1; i <= b; ++i) {
      std::set<std::string> tmps;
      elements[i]->getMatchingStrings(tmps);
      ns->clear();
      for (const std::string & o : *os) {
        for (const std::string & n : tmps) {
          ns->insert(o + n);
        }
      }
      std::swap(os, ns);
    }
    for (const std::string & o : *os) {
      s.insert(o);
    }
  }

  void getMatchingStrings(std::set<std::string> & s) const {
    getMatchingStrings(s, 0, elements.size() - 1);
  }

private:
  std::vector<std::shared_ptr<PtElement> > elements;
};

class PatternParser {
public:
  explicit PatternParser(std::string const & src_)
    : src(src_), pos(0), pstack(), current(),
      subExprCounter(1) {
    parse();
    postProcess();
  }
  std::shared_ptr<PtElement> getParsedPattern() {
    return current;
  }

  void linkToPattern(Pattern & p) {
    std::shared_ptr<PtKeywordSentinel> beginkw =
      std::make_shared<PtKeywordSentinel>(Pattern::begin_keyword_id);
    std::shared_ptr<PtKeywordSentinel> endkw =
      std::make_shared<PtKeywordSentinel>(Pattern::end_keyword_id);
    PtConcat sentinelpat;
    sentinelpat.addToEnd(beginkw);
    sentinelpat.addToEnd(current);
    sentinelpat.addToEnd(endkw);

    sentinelpat.findKeywords(p);
    p.finalizeKeywords();
    std::vector<PtKeyword*> dummy_kws;
    sentinelpat.calcFollowingKws(dummy_kws);
    sentinelpat.setFollowSets(p);
    PtBoundary *dummy_bptr = nullptr;
    sentinelpat.calcPreBoundaries(dummy_bptr, p);
    dummy_bptr = nullptr;
    sentinelpat.calcPostBoundaries(dummy_bptr, p);
    // Ending keywords are marked with end_keyword_id in the follow sets.
    Gap dummy_gap;
    sentinelpat.calcGaps(dummy_gap, p);

    // FIXME mark top-level gaps (or remove top-level optimization
    // from DynPmaReMatcher::traverseOutputPath)
    LOG("Pattern id#" << p.getId() << " after linking: " << current);
    if (p.isAlwaysFallback())
      LOG("Pattern id#" << p.getId() << " always requires fallback.");
  }

private:
  void parse();
  void postProcess();
  std::shared_ptr<PtElement> simplifyTwochar(std::shared_ptr<PtElement> e);
  int simplifyMakekws(std::shared_ptr<PtElement> e);
  void add(std::shared_ptr<PtElement> e);
  void readNext();
  void readKeyword();
  void readGap();
  void readRepeating();
  CharClass const * readCharClass();
  void readRepeatLength(gap_size_t & minLen, gap_size_t & maxLen);

  unsigned int createSubExprId() {
    return subExprCounter++;
  }

  /* Creates a pattern element matching a given set of keywords */
  std::shared_ptr<PtElement> ptFromKeywords(const std::set<std::string> & s) {
    ASSERT(s.size() != 0);
    if (s.size() == 1) {
      const std::string & kw = *s.cbegin();
      if (kw.size() == 0) {
        return std::make_shared<PtEmpty>();
      } else if (kw.size() == 1) {
        // Convert to gap with single-char character class (like -2)
        CharClass *kwcc = new CharClass;
        kwcc->add(kw);
        return std::make_shared<PtGap>(1, 1, kwcc->save());
      } else {
        return std::make_shared<PtKeyword>(kw);
      }
    } else {
      bool allonechar = true;
      for (const std::string & kw : s) {
        if (kw.size() != 1) {
          allonechar = false;
          break;
        }
      }
      if (allonechar) {
        // Convert to gap with character class
        CharClass *kwcc = new CharClass;
        for (const std::string & kw : s)
          kwcc->add(kw);
        return std::make_shared<PtGap>(1, 1, kwcc->save());
      } else {
        std::shared_ptr<PtOr> p = std::make_shared<PtOr>();
        for (const std::string & kw : s) {
          if (kw.size() == 0)
            p->addChoice(std::make_shared<PtEmpty>());
          else
            p->addChoice(std::make_shared<PtKeyword>(kw));
        }
        return p;
      }
    }
  }

  std::string src;
  size_t pos;
  std::stack<std::shared_ptr<PtElement> > pstack;
  std::shared_ptr<PtElement> current;
  unsigned int subExprCounter;
};

#endif // PM_PATTERNPARSER_HPP
