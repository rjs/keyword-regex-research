/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Parser for POSIX extended regular expressions (together with the
 * Pattern class which is used to store the result of parsing).
 */

#include "Pattern.hpp"
#include "PatternParser.hpp"
#include "RunMatcher.hpp"
#include <memory>
#include <string>
#include <stack>
#include <sstream>

using namespace std;

void PtKeyword::print(std::ostream & os) const {
  os << "(kw " << kwId << " "
     << (!preBoundary ? '-' : preBoundary->getType())
     << " \"" << keyword << "\" "
     << (!postBoundary ? '-' : postBoundary->getType())
     << ")";
}

void PtElement::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  if (immpre_b) {
    immpre_b->setNotFullyProcessedPre();
    immpre_b = nullptr;
  }
}
void PtElement::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  if (immpost_b) {
    immpost_b->setNotFullyProcessedPost();
    immpost_b = nullptr;
  }
}

void PtKeyword::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  if (immpre_b) {
    preBoundary = immpre_b;
    p.setPreBoundary(kwId, preBoundary->getType());
    immpre_b = nullptr;
  }
}

void PtKeyword::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  if (immpost_b) {
    postBoundary = immpost_b;
    p.setPostBoundary(kwId, postBoundary->getType());
    immpost_b = nullptr;
  }
}

void PtKeywordSentinel::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  if (immpre_b) {
    immpre_b->setNotFullyProcessedPre();
    immpre_b = nullptr;
  }
}

void PtKeywordSentinel::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  if (immpost_b) {
    immpost_b->setNotFullyProcessedPost();
    immpost_b = nullptr;
  }
}

void PtOr::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  vector<PtBoundary *> choicebs(choices.size(), immpre_b);
  for (size_t i = 0; i != choices.size(); ++i)
    choices[i]->calcPreBoundaries(choicebs[i], p);
  // ([a-z]\b|[0-9]\b)(foo|bar|[a-z]) => too complex to keep track of
  // which \bs to setNotFullyProcessed() later; so we just mark them
  // all with setNotFullyProcessed() and empty immpre_b.
  for (PtBoundary *b : choicebs)
    if (b)
      b->setNotFullyProcessedPre();
  immpre_b = nullptr;
}

void PtOr::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  vector<PtBoundary *> choicebs(choices.size(), immpost_b);
  for (ssize_t i = choices.size() - 1; i != -1; --i)
    choices[i]->calcPostBoundaries(choicebs[i], p);
  for (PtBoundary *b : choicebs) // reverse order does not matter
    if (b)
      b->setNotFullyProcessedPost();
  immpost_b = nullptr;
}

void PtRepeating::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  if (immpre_b) {
    immpre_b->setNotFullyProcessedPre();
    immpre_b = nullptr;
  }
  elt->calcPreBoundaries(immpre_b, p);
  if (immpre_b) {
    immpre_b->setNotFullyProcessedPre();
    immpre_b = nullptr;
  }
}

void PtRepeating::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  if (immpost_b) {
    immpost_b->setNotFullyProcessedPost();
    immpost_b = nullptr;
  }
  elt->calcPostBoundaries(immpost_b, p);
  if (immpost_b) {
    immpost_b->setNotFullyProcessedPost();
    immpost_b = nullptr;
  }
}

void PtBoundary::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  if (immpre_b)
    immpre_b->setNotFullyProcessedPre();
  immpre_b = this;
}

void PtBoundary::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  if (immpost_b)
    immpost_b->setNotFullyProcessedPost();
  immpost_b = this;
}

void PtConcat::calcPreBoundaries(PtBoundary *&immpre_b, Pattern & p) {
  for (auto & e : elements)
    e->calcPreBoundaries(immpre_b, p);
}

void PtConcat::calcPostBoundaries(PtBoundary *&immpost_b, Pattern & p) {
  for (auto it = elements.rbegin(); it != elements.rend(); ++it) {
    (*it)->calcPostBoundaries(immpost_b, p);
  }
}

void PatternParser::add(shared_ptr<PtElement> e) {
  // LOG("parse add: " << e);
  if (!current) {
    current = e;
  } else if (dynamic_pointer_cast<PtConcat>(current) != 0) {
    dynamic_pointer_cast<PtConcat>(current)->addToEnd(e);
  } else {
    current = make_shared<PtConcat>(current);
    dynamic_pointer_cast<PtConcat>(current)->addToEnd(e);
  }
}

void PatternParser::parse() {
  ASSERT(pos == 0);
  ASSERT(src.length() > 0);
  ASSERT(pstack.empty());
  ASSERT(!current);

  while (pos < src.length()) {
    readNext();
  }

  if (!pstack.empty() && dynamic_pointer_cast<PtOr>(pstack.top()) != 0) {
    dynamic_pointer_cast<PtOr>(pstack.top())->addChoice(current);
    current = pstack.top();
    pstack.pop();
  }

  ASSERT(pstack.empty());
  ASSERT(!!current);

  LOG("Parsed pattern: " << current);
}

void PatternParser::postProcess() {
  ASSERT(!!current);

  if (::RunEnv::isFlagSet(RUN_FLAG_MAKEKWS)) {
    int eltCount = simplifyMakekws(current);
    if (eltCount > 0) {
      set<string> s;
      current->getMatchingStrings(s);
      current = ptFromKeywords(s);
    }
  }

  if (::RunEnv::isFlagSet(RUN_FLAG_TWO_CHAR_KEYWORDS))
    current = simplifyTwochar(current);
}

shared_ptr<PtElement> PatternParser::simplifyTwochar(shared_ptr<PtElement> e) {
  if (dynamic_pointer_cast<PtKeyword>(e) != 0 &&
      dynamic_pointer_cast<PtKeyword>(e)->getKeyword().length() == 1) {
    // Convert to gap with single-char character class
    const string & kw = dynamic_pointer_cast<PtKeyword>(e)->getKeyword();
    CharClass *kwcc = new CharClass;
    kwcc->add(kw);
    return make_shared<PtGap>(1, 1, kwcc->save());
  }

  if (dynamic_pointer_cast<PtOr>(e) != 0) {
    shared_ptr<PtOr> ee = dynamic_pointer_cast<PtOr>(e);
    for (size_t i = 0; i != ee->choices.size(); ++i)
      ee->choices[i] = simplifyTwochar(ee->choices[i]);
  } else if (dynamic_pointer_cast<PtConcat>(e) != 0) {
    shared_ptr<PtConcat> ee = dynamic_pointer_cast<PtConcat>(e);
    for (size_t i = 0; i != ee->elements.size(); ++i)
      ee->elements[i] = simplifyTwochar(ee->elements[i]);
  } else if (dynamic_pointer_cast<PtRepeating>(e) != 0) {
    shared_ptr<PtRepeating> ee = dynamic_pointer_cast<PtRepeating>(e);
    ee->elt = simplifyTwochar(ee->elt);
  } else if (dynamic_pointer_cast<PtSubExpr>(e) != 0) {
    shared_ptr<PtSubExpr> ee = dynamic_pointer_cast<PtSubExpr>(e);
    ee->elt = simplifyTwochar(ee->elt);
  }

  return e;
}

static const size_t maxMatchingStrings = 100;

static int repeatingChoices(int choices, gap_size_t minr, gap_size_t maxr) {
  if (maxr == Gap::infinite || choices <= 0)
    return -1;

  if (choices == 1)
    return maxr - minr + 1;

  /*
   * c choices a=minr .. b=maxr times => total choices
   *   c^a + c^{a+1} + ... + c^b
   * = c^a \sum{i=0}^{b-a} c^i
   * = c^a * (c^{b-a+1} - 1) / (c - 1)
   */
  // FIXME use e.g. floating-point pow(3) to make this faster?
  size_t a = minr, b = maxr, c = choices;
  size_t c_to_a = 1;
  for (size_t i = 0; i < a; ++i) {
    c_to_a *= c;
    if (c_to_a > maxMatchingStrings)
      return -1;
  }

  size_t c_to_b_a_1 = 1;
  for (size_t i = 0; i < b - a + 1; ++i) {
    c_to_b_a_1 *= c;
    if (c_to_b_a_1 > maxMatchingStrings)
      return -1;
  }

  size_t count = c_to_a * ((c_to_b_a_1 - 1) / (c - 1));
  if (count > maxMatchingStrings)
    return -1;
  return count;
}

// Returns estimate on size of e->getMatchingStrings() (or -1 = too much)
int PatternParser::simplifyMakekws(shared_ptr<PtElement> e) {
  if (dynamic_pointer_cast<PtGap>(e) != 0) {
    shared_ptr<PtGap> ee = dynamic_pointer_cast<PtGap>(e);
    int eeCount = repeatingChoices(ee->cc ? ee->cc->count()
                                   : CharClass::dotCC->count(),
                                   ee->minLength,
                                   ee->maxLength);
    if (eeCount < 0) {
      // FIXME optimize by cutting some off if minLength > 0?
      return -1;
    } else {
      return eeCount;
    }
  } else if (dynamic_pointer_cast<PtOr>(e) != 0) {
    shared_ptr<PtOr> ee = dynamic_pointer_cast<PtOr>(e);
    vector<int> eltCounts(ee->choices.size());
    bool passthrough = true;
    int sum = 0;
    for (size_t i = 0; i != ee->choices.size(); ++i) {
      eltCounts[i] = simplifyMakekws(ee->choices[i]);
      if (eltCounts[i] < 0) {
        passthrough = false;
      } else {
        if (sum <= static_cast<int>(maxMatchingStrings))
          sum += eltCounts[i];
        else
          passthrough = false;
      }
    }
    if (passthrough)
      return sum;
    if (sum <= 0)
      return -1;
    // Don't care about sum going above maxMatchingStrings, since this
    // is an Or anyway.
    set<string> s;
    vector<shared_ptr<PtElement> > remainingchoices;
    for (size_t i = 0; i != ee->choices.size(); ++i) {
      if (eltCounts[i] > 0) {
        ee->choices[i]->getMatchingStrings(s);
      } else {
        remainingchoices.push_back(ee->choices[i]);
      }
    }
    shared_ptr<PtElement> kwor = ptFromKeywords(s);
    ee->choices.assign(remainingchoices.cbegin(), remainingchoices.cend());
    ee->choices.push_back(kwor);
    return -1;
  } else if (dynamic_pointer_cast<PtConcat>(e) != 0) {
    shared_ptr<PtConcat> ee = dynamic_pointer_cast<PtConcat>(e);
    vector<int> eltCounts(ee->elements.size());
    vector<shared_ptr<PtElement> > new_elements;
    size_t i;
    int prod = 1;
    for (i = 0; i != ee->elements.size(); ++i) {
      eltCounts[i] = simplifyMakekws(ee->elements[i]);
      if (prod > 0 && eltCounts[i] > 0 &&
          prod * eltCounts[i] < static_cast<int>(maxMatchingStrings))
        prod *= eltCounts[i];
      else
        prod = -1;
    }
    if (prod > 0)
      return prod;
    i = 0;
    while (i < ee->elements.size()) {
      if (eltCounts[i] <= 0) {
        new_elements.push_back(ee->elements[i]);
        ++i;
      } else {
        // Greedy approach: take as many elements following i as
        // possible w.r.t. maxMatchingStrings
        size_t j = i + 1;
        int prod = eltCounts[i];
        for (; j < ee->elements.size(); ++j) {
          if (eltCounts[j] <= 0 ||
              prod * eltCounts[j] > static_cast<int>(maxMatchingStrings))
            break;
          prod *= eltCounts[j];
        }
        --j;
        // Make keywords from elements i to j
        set<string> s;
        ee->getMatchingStrings(s, i, j);
        new_elements.push_back(ptFromKeywords(s));
        i = j + 1;
      }
    }
    ee->elements = new_elements;
    return -1;
  } else if (dynamic_pointer_cast<PtRepeating>(e) != 0) {
    shared_ptr<PtRepeating> ee = dynamic_pointer_cast<PtRepeating>(e);
    int eltCount = simplifyMakekws(ee->elt);
    int eeCount = repeatingChoices(eltCount,
                                   ee->minr,
                                   ee->maxr);
    if (eeCount < 0) {
      // FIXME optimize by cutting some off if minLength > 0?
      return -1;
    } else {
      return eeCount;
    }
  } else if (dynamic_pointer_cast<PtSubExpr>(e) != 0) {
    shared_ptr<PtSubExpr> ee = dynamic_pointer_cast<PtSubExpr>(e);
    int eltCount = simplifyMakekws(ee->elt);
    return eltCount;
    /*
     * FIXME the above can remove subexpressions even if there are backreferences
     * If there are backreferences to this PtSubExpr, this should be:
     * if (eltCount > 0) {
     *   set<string> s;
     *   ee->elt->getMatchingStrings(s);
     *   ee->elt = ptFromKeywords(s);
     * }
     * return -1;
     */
  } else if (dynamic_pointer_cast<PtEmpty>(e) != 0) {
    return 1;
  } else if (dynamic_pointer_cast<PtKeyword>(e) != 0) {
    return 1;
  } else {                      // PtBoundary or PtBackreference
    return -1;
  }
}

void PatternParser::readNext() {
  ASSERT(pos < src.length());

  // Remember to add reserved characters to the readKeyword() function below
  switch (src[pos]) {
  case '(':
    pos++;
    pstack.push(current);
    current = shared_ptr<PtElement>();
    return;
  case ')':
    pos++;
    ASSERT(!pstack.empty());
    if (dynamic_pointer_cast<PtOr>(pstack.top()) != 0) {
      dynamic_pointer_cast<PtOr>(pstack.top())->addChoice(current);
      current = pstack.top();
      pstack.pop();
    }
    ASSERT(!pstack.empty());
    {
      shared_ptr<PtElement> e = current;
      current = pstack.top();
      pstack.pop();
      // At ')', so nested subexpressions are numbered from the linear
      // order of their ')' characters, which is how some greps work
      add(make_shared<PtSubExpr>(e, createSubExprId()));
      return;
    }
  case '|':
    pos++;
    if (!pstack.empty() && dynamic_pointer_cast<PtOr>(pstack.top()) != 0) {
      dynamic_pointer_cast<PtOr>(pstack.top())->addChoice(current);
    } else {
      pstack.push(make_shared<PtOr>(current));
    }
    current = shared_ptr<PtElement>();
    return;
  case '^':
  case '$':
    add(make_shared<PtBoundary>(src[pos++]));
    return;
  case '.':
    readGap();
    return;
  case '[':
    if (pos + 2 < src.length() &&
        src[pos+1] != '^' &&
        src[pos+2] == ']') // [x]-style "escape"
      readKeyword();
    else
      readGap();
    return;
  case '?':
  case '*':
  case '+':
  case '{':
    readRepeating();
    return;
  case ']':
  case '}':
    ERROR("Invalid pattern syntax (] or } in wrong place)!");
  case '\\':
    if (pos + 1 >= src.length())
      ERROR("Invalid pattern syntax (\\ at end of pattern)!");
    switch (src[pos + 1]) {
    case 'b':
    case 'B':
    case '<':
    case '>':
      pos++;
      add(make_shared<PtBoundary>(src[pos++]));
      return;
    case '1': case '2': case '3': case '4': case '5':
    case '6': case '7': case '8': case '9':
      pos++;
      add(make_shared<PtBackreference>(src[pos++] - '0'));
      return;
    case 'w':
    case 'W':
      readGap();
      return;
    default:                // escaped character in keyword
      if (isalnum(src[pos + 1]))
        ERROR("Invalid pattern syntax (unknown \\x escape)!");
      readKeyword();
      return;
    }
    break;
  default:
    readKeyword();
    return;
  }
}

void PatternParser::readKeyword() {
  ostringstream ss;
  size_t p = pos;

  while (pos < src.length()) {
    p = src.find_first_of("^$()|.?*+{}[]\\", pos);
    if (p == src.npos) {
      p = src.length();
      break;
    }
    if (src[p] == '\\') {
      if (p + 1 >= src.length())
        ERROR("Invalid pattern syntax (\\ at end of pattern)!");
      if (isalnum(src[p + 1])   // e.g., \b or \1
          || src[p + 1] == '<' || src[p + 1] == '>') {
        // stop keyword at p
        break;
      }
      ss << src.substr(pos, p - pos);
      ss << src[p + 1];
      pos = p + 2;
      p = pos;
    } else if (src[p] == '[' &&
               p + 2 < src.length() &&
               src[p+1] != '^' &&
               src[p+2] == ']') { // [x]-style "escape"
      ss << src.substr(pos, p - pos);
      ss << src[p + 1];
      pos = p + 3;
      p = pos;
    } else {
      break;
    }
  }

  ss << src.substr(pos, p - pos);
  pos = p;

  string s = ss.str();
  if (s.length() > 1 && pos < src.length() &&
      (src[pos] == '?' || src[pos] == '*' ||
       src[pos] == '+' || src[pos] == '{')) {
    // Split last char out from abc*
    // (=> PtKeyword(ab), PtRepeating(PtKeyword c))
    shared_ptr<PtKeyword> kw1 =
      make_shared<PtKeyword>(s.substr(0, s.length() - 1));
    shared_ptr<PtKeyword> kw2 =
      make_shared<PtKeyword>(s.substr(s.length() - 1));
    gap_size_t minLen, maxLen;
    readRepeatLength(minLen, maxLen);
    add(kw1);
    add(make_shared<PtRepeating>(kw2, minLen, maxLen));
  } else {
    add(make_shared<PtKeyword>(ss.str()));
  }
}

void PatternParser::readRepeating() {
  if (!current)
    ERROR("Repeat command at beginning; invalid pattern");
  gap_size_t minLen, maxLen;
  readRepeatLength(minLen, maxLen);

  shared_ptr<PtConcat> cc = dynamic_pointer_cast<PtConcat>(current);
  if (cc != 0) {
    // Wrap last part of PtConcat current in PtRepeating
    cc->replace(cc->nElements() - 1,
                make_shared<PtRepeating>(cc->getElement(cc->nElements() - 1),
                                         minLen,
                                         maxLen));
  } else {
    // Wrap current in PtRepeating
    current = make_shared<PtRepeating>(current, minLen, maxLen);
  }
}

// a single character-class specification . or \w or \W or [...]
CharClass const * PatternParser::readCharClass() {
  if (src[pos] == '.') {
    pos++;
    return nullptr;
  }

  if (src[pos] == '\\') {
    ASSERT(pos + 1 < src.length());
    char typech = src[pos + 1];
    pos += 2;
    return CharClass::predef_class(typech);
  }

  CharClass *cc = new CharClass;
  bool reversed = false;

  ASSERT(src[pos] == '[');
  pos++;
  ASSERT(pos + 1 < src.length());

  if (src[pos] == '^') {
    reversed = true;
    pos++;
  }

  if (src[pos] == ']') {        // first char is ]
    cc->add(src[pos]);
    pos++;
  }

  while (pos < src.length() && src[pos] != ']') {
    if (pos + 2 < src.length() && src[pos+1] == '-' && src[pos+2] != ']') {
      cc->add_range(src[pos], src[pos+2]);
      pos += 3;
    } else {
      cc->add(src[pos]);
      pos++;
    }
  }

  ASSERT(src[pos] == ']');
  pos++;

  if (reversed)
    cc->reverse();

  return cc->save();
}

// a string containing one . or \[wW] or [] and possibly a length * ? + {}
void PatternParser::readGap() {
  CharClass const * cc = readCharClass();
  gap_size_t minLen, maxLen;
  readRepeatLength(minLen, maxLen);
  add(make_shared<PtGap>(minLen, maxLen, cc));
}

void PatternParser::readRepeatLength(gap_size_t & minLen, gap_size_t & maxLen) {
  minLen = 1;
  maxLen = 1;
  if (src.length() <= pos)
    return;

  if (src[pos] == '*') {
    minLen = 0;
    maxLen = Gap::infinite;
    ++pos;
    return;
  } else if (src[pos] == '?') {
    minLen = 0;
    maxLen = 1;
    ++pos;
    return;
  } else if (src[pos] == '+') {
    minLen = 1;
    maxLen = Gap::infinite;
    ++pos;
    return;
  }

  if (src[pos] != '{')
    return;

  // Minimum "{}" gap length specification is 3: "{0}"
  if (src.length() <= pos + 2)
    ERROR("Invalid pattern syntax ({ too late)!");

  size_t p = src.find_first_of(",}", pos + 1);
  if (p == src.npos)
    ERROR("Invalid pattern syntax ({ without , or })!");

  minLen = atoi(src.substr(pos + 1, p - pos - 1).c_str());
  if (src[p] == ',') {
    size_t q = src.find_first_of('}', p + 1);
    if (q == src.npos)
      ERROR("Invalid pattern syntax ({..., without })!");
    maxLen = (q == p + 1) ? Gap::infinite :
      atoi(src.substr(p + 1, q - p - 1).c_str());
    pos = q + 1;
  } else {
    ASSERT(src[p] == '}');
    maxLen = minLen;
    pos = p + 1;
  }
}
