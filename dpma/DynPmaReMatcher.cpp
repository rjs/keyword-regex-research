/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * The main part of the matching algorithm described in "Experimental
 * Analysis of an Online Dictionary Matching Algorithm for Regular
 * Expressions with Gaps" (SEA 2015).
 */

#include "DynPmaReMatcher.hpp"
#include "Reader.hpp"
#include "Utils.hpp"
#include "util-Collections.hpp"
#include "util-Log.hpp"
#include <queue>
#include <cstring>

using namespace std;

#define INITIAL_STATE 0
#define NONEXISTENT_STATE (static_cast<unsigned int>(-1))

/*
 * This optional optimization adds fail transitions directly to the
 * transition tables of each state, avoiding the need to traverse the
 * fail chain during matching. Uncomment the define to disable it.
 */
#define FAILS_TO_GOTOS

DynamicPmaReMatcher::DynamicPmaReMatcher(vector<Pattern> & patterns)
  : Matcher(patterns), maxDist(0), maxKWLen(0), newState(INITIAL_STATE),
    nStates(0), gotos(0), usedChars(), fails(0), outputFails(0),
#ifdef USE_PENDING_OUTPUT
    pendingOutput(0),
#endif
    currentOutputs(0),
    initial_currentOutputs(0),
    maxKWFound(0), needFallback(0), kwState(0),
    stateAge(0), patAge(0) {

  countMaxDistAndKWLen();
  ASSERT(maxDist > 0);
  ASSERT(maxKWLen > 0);
  kwState = new unsigned int*[patterns.size()];
  maxKWFound = new int[patterns.size()];
  needFallback = new bool[patterns.size()];
  patAge = new size_t[patterns.size()];

  for (size_t i = 0; i < patterns.size(); i++) {
    kwState[i] = new unsigned int[patterns[i].getNumKeywords()];
    maxKWFound[i] = -1;
    needFallback[i] = patterns[i].isAlwaysFallback();
    patAge[i] = 0;
  }

  vector<map<chartype, unsigned int> > pgotos;
  pgotos.push_back(map<chartype, unsigned int> ());
  for (size_t patId = 0; patId < patterns.size(); patId++) {
    Pattern const & pattern = patterns[patId];
    for (size_t kwId = 0; kwId < pattern.getNumKeywords(); kwId++) {
      addKeyword(pattern.getKeyword(kwId), pgotos, patId, kwId);
    }
  }
  nStates = newState + 1;
  ASSERTEQUALS(nStates, pgotos.size());

  fails = new unsigned int[nStates];
  outputFails = new unsigned int[nStates];
  constructFailStates(pgotos);
  constructStaticOutputFails();

#ifdef FAILS_TO_GOTOS
  addFailsToGotos(pgotos);
#endif

  stateAge = new size_t[nStates];
  gotos = new TransitionTable<chartype, unsigned int> [nStates];
  for (size_t i = 0; i < nStates; i++) {
#ifdef FAILS_TO_GOTOS
    gotos[i].set_all(pgotos[i], INITIAL_STATE);
#else
    gotos[i].set_all(pgotos[i],
                     i == INITIAL_STATE
                     ? INITIAL_STATE : NONEXISTENT_STATE);
#endif
    stateAge[i] = 0;
  }

  ASSERTEQUALS((size_t) newState + 1, nStates);
  currentOutputs = new OutputSetType[nStates];
  initial_currentOutputs = new OutputSetType[nStates];
#ifdef USE_PENDING_OUTPUT
  pendingOutput = new PendingSetType[maxDist];
#endif

  LOG("PMA: " << patterns.size() << " patterns, "
      << nStates << " states, maxDist = " << maxDist);
}

DynamicPmaReMatcher::~DynamicPmaReMatcher() {
  for (size_t i = 0; i < Matcher::getNumPatterns(); i++) {
    delete[] kwState[i];
  }
  delete[] kwState;
  delete[] fails;
  delete[] outputFails;
  delete[] currentOutputs;
  delete[] initial_currentOutputs;
#ifdef USE_PENDING_OUTPUT
  delete[] pendingOutput;
#endif
  delete[] gotos;
}

void DynamicPmaReMatcher::findMatches(Reader *src_) {
  src = src_;

  initializeOutput();

  unsigned int state = INITIAL_STATE;
  chartype nextChar;

  charCount = 0;

  try {
    for (;;) {
      nextChar = static_cast<chartype>(src->readChar());
      LOG("Read " << static_cast<int>(nextChar) << " (" << nextChar << ")");
      // Matcher::stats.logOperation(Statistics::ProcessChar);
      ASSERT(nextChar < 256);
      ASSERT(nextChar >= 0);
      charCount++;
      if (nextChar == '\n') {
        if (lineMode)
          runFallbacks(true);
        lineBreak();
#ifdef USE_PENDING_OUTPUT
        if (lineMode)
          resetPendingOutput();
#endif
      }
      if (charCount % 256 == 0)
        runFallbacks(false);

#ifdef USE_PENDING_OUTPUT
      distributeOutput();
#endif

#ifdef FAILS_TO_GOTOS
      state = gotos[state].get(nextChar);
#else
      unsigned int s;
      while ((s = gotos[state].get(nextChar)) == NONEXISTENT_STATE)
        state = fails[state];
      state = s;
#endif

      traverseOutputPath(state);
    }
  } catch (Reader::end_of_data &e) {
    ;
  } catch (Matcher::stop_matching &e) {
    ;
  }

  src = nullptr;
}

unsigned int DynamicPmaReMatcher::outputFail(unsigned int q) {
  unsigned int state = outputFails[q];

  while (state != INITIAL_STATE) {
    checkAge_state(state);
    if (!currentOutputs[state].empty())
      break;
    state = outputFails[state];
  }

  ASSERT(state == INITIAL_STATE || !currentOutputs[state].empty());
  return state;
}

#ifdef USE_PENDING_OUTPUT
void DynamicPmaReMatcher::distributeOutput() {
  PendingSetType & list = pendingOutput[Matcher::charCount % maxDist];
  for (const PendingTuple & po : list) {
    unsigned int const patId = po.pattern();
    unsigned int const keywordId = po.keyword();
    // We can skip this output if this keyword has already been found
    // (and has a star gap after it), because processing it again
    // serves no purpose
    checkAge_pat(patId);
    if (static_cast<int>(keywordId) < maxKWFound[patId])
      continue;
    // If we stop on first match found and pattern has been found,
    // discard this tuple
    if (Matcher::stopOnFirst && Matcher::getPattern(patId).isFound())
      continue;

    ASSERT(patId < Matcher::getNumPatterns());
    ASSERT(keywordId < Matcher::getPattern(patId).getNumKeywords());
    distributeOutputTuple(po);
  }
  list.clear();
}
#endif

void DynamicPmaReMatcher::addKeyword(std::string const & keyword,
                                     vector<map<chartype, unsigned int> > & pgotos,
                                     size_t patId, size_t kwId) {
  if (keyword == "") {
    kwState[patId][kwId] = 0;
    return;
  }
  unsigned int state = 0;
  unsigned int j = 0;
  map<chartype, unsigned int>::const_iterator existing;
  while (j < keyword.length() &&
         (existing = pgotos[state].find(keyword[j])) != pgotos[state].cend()) {
    state = (*existing).second;
    j++;
  }
  for (size_t p = j; p < keyword.length(); p++) {
    // If there are multiple identical keywords, then this loop might
    // not be run
    newState++;
    pgotos.push_back(map<chartype, unsigned int> ());
    pgotos[state][keyword[p]] = newState;
    usedChars.insert(keyword[p]);
    state = newState;
    ASSERTEQUALS((size_t) newState + 1, pgotos.size());
  }
  // state is the state where keyword leads to
  kwState[patId][kwId] = state;
  if (!keywordState[state]) {
    // Mark state as a keyword state
    Matcher::stats.logOperation(Statistics::KeywordState);
    keywordState[state] = true;
  }
}

void DynamicPmaReMatcher::constructFailStates(
  vector<map<chartype,unsigned int> > const & pgotos) {
  queue<unsigned int> queue;
  fails[INITIAL_STATE] = INITIAL_STATE;
  for (chartype a : usedChars) {
    if (pgotos[INITIAL_STATE].find(a) != pgotos[INITIAL_STATE].cend()) {
      unsigned int s = pgotos[INITIAL_STATE].find(a)->second;
      queue.push(s);
      fails[s] = INITIAL_STATE;
    }
  }
  while (!queue.empty()) {
    unsigned int r = queue.front();
    queue.pop();
    for (chartype a : usedChars) {
      if (pgotos[r].find(a) != pgotos[r].cend()) {
        unsigned int s = pgotos[r].find(a)->second;
        queue.push(s);
        unsigned int state = fails[r];
        while (state != INITIAL_STATE &&
               pgotos[state].find(a) == pgotos[state].cend()) {
          state = fails[state];
        }
        if (pgotos[state].find(a) == pgotos[state].cend())
          fails[s] = INITIAL_STATE;
        else
          fails[s] = pgotos[state].find(a)->second;
      }
    }
  }
}

void DynamicPmaReMatcher::constructStaticOutputFails() {
  // Initialize values to unset (if outputFail[i] == i, then the value
  // is unset, unless i = 0)
  for (size_t i = 0; i < nStates; i++) {
    outputFails[i] = i;
  }
  // State 0 = INITIAL_STATE is left as it is, so that outputFails[0] = 0
  for (size_t i = 1; i < nStates; i++) {
    if (outputFails[i] == i) {
      // Not yet processed
      outputFails[i] = findStaticOutputFail(i);
      ASSERT(outputFails[i] != i);
    }
  }
}

unsigned int DynamicPmaReMatcher::findStaticOutputFail(unsigned int state) {
  unsigned int parent = fails[state];
  if (parent == INITIAL_STATE)
    return INITIAL_STATE;

  if (outputFails[parent] == parent) {
    outputFails[parent] = findStaticOutputFail(parent);
    ASSERT(outputFails[parent] != parent);
  }

  unsigned int outputFail = keywordState[parent] ? parent
    : outputFails[parent];
  ASSERT(outputFail == 0 || keywordState[outputFail]);
  return outputFail;
}

void DynamicPmaReMatcher::addFailsToGotos(
  vector<map<chartype,unsigned int> > & pgotos) {
  for (size_t i = 0; i < nStates; i++) {
    size_t s = i;
    do {
      s = fails[s];
      for (auto p : pgotos[s]) {
        chartype c = p.first;
        unsigned int v = p.second;
        if (pgotos[i].find(c) == pgotos[i].cend()) {
          pgotos[i][c] = v;
        }
      }
    } while (s != INITIAL_STATE);
  }
}

#ifdef USE_PENDING_OUTPUT
void DynamicPmaReMatcher::resetPendingOutput() {
  for (size_t i = 0; i < maxDist; i++) {
    pendingOutput[i].clear();
  }

  if (!Matcher::lineMode) {
    // Insert initial tuples into pendingOutput
    for (size_t i = 0; i < Matcher::getNumPatterns(); i++) {
      const Pattern & p = Matcher::getPattern(i);
      for (keywordid_t kwId : p.getBeginSet()) {
        size_t b = (p.getGap(kwId).getMinLength() +
                    p.getKeyword(kwId).length());
        size_t e = (p.getGap(kwId).getMaxLength() == Gap::infinite) ?
          PendingTuple::no_end :
          p.getGap(kwId).getMaxLength() + p.getKeyword(kwId).length();
        Matcher::stats.logOperation(Statistics::InsertPendingOutput);
        OP_INSERT(pendingOutput[b % maxDist],
                  PendingTuple(b, i, kwId, e));
      }
    }
  }
}
#endif

void DynamicPmaReMatcher::initializeOutput() {
#ifdef USE_PENDING_OUTPUT
  resetPendingOutput();
#endif

  for (size_t i = 0; i < nStates; i++) {
    currentOutputs[i].clear();
  }

#ifdef USE_PENDING_OUTPUT
  if (Matcher::lineMode) {
#endif
    // Insert initial tuples directly into currentOutputs
    for (size_t i = 0; i < Matcher::getNumPatterns(); i++) {
      const Pattern & p = Matcher::getPattern(i);
      for (keywordid_t kwId : p.getBeginSet()) {
        size_t b = (p.getGap(kwId).getMinLength() +
                    p.getKeyword(kwId).length());
        size_t e = (p.getGap(kwId).getMaxLength() == Gap::infinite) ?
          PendingTuple::no_end :
          p.getGap(kwId).getMaxLength() + p.getKeyword(kwId).length();
        Matcher::stats.logOperation(Statistics::DistributeOutput);
        unsigned int q = kwState[i][kwId];
        OP_INSERT(currentOutputs[q], OutputTuple(b, i, kwId, e));
      }
    }

    for (size_t i = 0; i < nStates; ++i) {
      initial_currentOutputs[i] = currentOutputs[i];
    }
#ifdef USE_PENDING_OUTPUT
  }
#endif
}

void DynamicPmaReMatcher::countMaxDistAndKWLen() {
  maxKWLen = 0;
  maxDist = 0;
  for (size_t p = 0; p < Matcher::getNumPatterns(); p++) {
    Pattern const & pattern = Matcher::getPattern(p);
    for (size_t i = 0; i < pattern.getNumKeywords(); i++) {
      size_t kwLen = pattern.getKeyword(i).length();
      if (kwLen > maxKWLen)
        maxKWLen = kwLen;
      size_t dist = pattern.getGap(i).getMinLength() + kwLen;
      if (dist > maxDist)
        maxDist = dist;
    }
  }
}

/*
 * Custom output-path traversal for the regular-expression DPMA
 */
void DynamicPmaReMatcher::traverseOutputPath(unsigned int q) {
  for (;;) {
    checkAge_state(q);

    OutputSetType & c = currentOutputs[q];
    LOG("There are " << c.size() << " current outputs");

    for (OutputSetType::iterator it = c.begin(); it != c.end();OP_FOREACH(it)) {
      stats.logOperation(Statistics::ProcessCurrentOutput);
      OutputTuple const & po = *it;
      unsigned int const patId = po.pattern();
      unsigned int const keywordId = po.keyword();
      Pattern const & pat = getPattern(patId);

      checkAge_pat(patId);
      if (Matcher::lineMode && charCount < po.b()) {
        LOG("Skipping over an output tuple from the future");
        OP_FOREND(it);
        continue;
      }

      LOG("Partial match: " << pat.getKeyword(keywordId)
          << "(" << maxKWFound[patId] << "); " << po
          << " at char count " << charCount);
      stats.logOperation(Statistics::KeywordMatch);
      if (stopOnFirst && pat.isFound()) {
        // We are asked to stop on first match, and pattern has
        // already been found
        LOG("Discarding tuple because pattern already found");
        OP_ERASE(c, it);
        continue;
      }
      if (po.e() < charCount || maxKWFound[patId] >= (int) keywordId) {
        LOG("Discarding partial match");
        OP_ERASE(c, it);
        continue;
      }

      // FIXME optimize this by saving eolChar in Matcher?
      chartype eolChar = lineMode ? '\n' : '\0';
      if (pat.hasPreBoundary(keywordId) &&
          !pat.matchPreBoundary(keywordId,
            src->prevChar_or(pat.getKeyword(keywordId).length() + 1, eolChar))) {
        LOG("Discarding keyword match because preBoundary does not match");
        OP_FOREND(it);
        continue;
      }

      if (pat.hasPostBoundary(keywordId) &&
          !pat.matchPostBoundary(keywordId, src->aheadChar_or(0, eolChar))) {
        LOG("Discarding keyword match because postBoundary does not match");
        OP_FOREND(it);
        continue;
      }

      bool & needfb = needFallback[patId];
      // The gap begins at the match position of the previous keyword
      // (= charCount when the pending/output tuple was added)
      size_t prevKwEndPos = po.b() - pat.getGap(keywordId).getMinLength() -
        pat.getKeyword(keywordId).length();
      ReadString gapstr = src->prevChars(charCount - prevKwEndPos,
                                         pat.getKeyword(keywordId).length());
      if (!pat.getGap(keywordId).matches(gapstr, needfb)) {
        LOG("Discarding keyword match because gap does not match");
        OP_FOREND(it);
        continue;
      }
      if (pat.isBeginningKeyword(keywordId)) {
        if (lineMode ? matchBegin[patId] < bolPos : matchBegin[patId] != 0)
          // This is always at bolPos or 0 because of initial .* gap
          matchBegin[patId] = prevKwEndPos;
      }
      if (pat.isEndingKeyword(keywordId)) {
        // Possible match at position charCount - 1
        if (needfb) {
          setupOrExecuteFallback(patId);
        } else {
          reportMatch(charCount - 1, patId);
        }
      }
      set<keywordid_t> const & followSet = pat.getFollowSet(keywordId);
      bool needRemove = false;
      for (keywordid_t k : followSet) {
        if (k == Pattern::end_keyword_id)
          continue;

        string const & nkw = pat.getKeyword(k);
        Gap const & ngap = pat.getGap(k);
        size_t bp = charCount + ngap.getMinLength() + nkw.length();
        size_t ep = ngap.getMaxLength();

        if (ep != Gap::infinite) {
          ep += nkw.length() + charCount;
        } else {
          ep = PendingTuple::no_end;
          if (ngap.isTopLevel()) {
            // We have located a keyword with an
            // endpoint-infinite top-level gap following
            // it. Mark that this keyword has been found.
            // FIXME needs to check that the gap has no character
            // classes except in ccrBegin!
            maxKWFound[patId] = keywordId;
          }
        }

        PendingTuple ntup(bp, patId, k, ep);
        LOG("Adding pending output " << ntup << " for <"
            << pat.getKeyword(ntup.keyword()) << "> to position "
            << bp << "; currently at " << charCount);
        stats.logOperation(Statistics::InsertPendingOutput);
#ifdef USE_PENDING_OUTPUT
        if (bp > charCount) {
          OP_INSERT(pendingOutput[bp % maxDist], ntup);
        } else {
          // Special case: empty keyword, min-zero-gap, need to
          // distribute directly
          distributeOutputTuple(ntup);
        }
#else
        distributeOutputTuple(ntup);
#endif
        if (ep == PendingTuple::no_end && followSet.size() == 1) {
          ASSERT(!pat.isEndingKeyword(keywordId)); // since followSet has only k
          stats.logOperation(Statistics::SegmentMatch);
          // Delete this output tuple from the current output set
          // Have to call continue outside this for loop
          needRemove = true;
        }
      }
      if (needRemove) {
        OP_ERASE(c, it);
        continue;
      }
      // Didn't erase current element; so increase it (if necessary
      // for the set type)
      OP_FOREND(it);
    }

    if (q == INITIAL_STATE) {
      return;
    } else {
      q = outputFail(q);
    }
  }
}
