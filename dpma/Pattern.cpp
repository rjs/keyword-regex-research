/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Classes for representing parsed regular expression patterns using
 * character classes, keywords and gaps.
 */

#include "Pattern.hpp"
#include "PatternParser.hpp"
#include "util-Log.hpp"
#include <limits>
#include <cstdlib>
#include <sstream>
#include <vector>
#include <algorithm>
#include <memory>

using std::ostringstream;
using std::vector;
using std::set;
using std::pair;
using std::string;
using std::find_if;
using std::shared_ptr;

std::vector<const CharClass *> CharClass::allCharClasses;
const CharClass *CharClass::wordCC, *CharClass::nonWordCC;
const CharClass *CharClass::eolCC, *CharClass::bothEolCC;
const CharClass *CharClass::dotCC;

// may invalidate the CharClass object! (use the return value instead)
// the object must have been allocated with new!
const CharClass *CharClass::save() {
  ASSERT(!saved);
  auto it = find_if(allCharClasses.cbegin(), allCharClasses.cend(),
                    [this](const CharClass *c){ return *this==*c; });
  if (it == allCharClasses.cend()) {
    saved = true;
    allCharClasses.push_back(this);
    LOG("Saved CharClass with " << chars.count() << " characters.");
    return this;
  } else {
    delete this;
    return *it;
  }
}

void CharClass::init(chartype eolChar) {
  CharClass *word = new CharClass;
  CharClass *nonWord = new CharClass;
  CharClass *eol = new CharClass;
  CharClass *bothEol = new CharClass;
  CharClass *dot = new CharClass;

  // Word character == [_[:alnum:]] == [_A-Za-z0-9]
  word->add_range('0', '9');
  word->add_range('A', 'Z');
  word->add_range('a', 'z');
  word->add('_');
  wordCC = word->save();

  nonWord->add_range('0', '9');
  nonWord->add_range('A', 'Z');
  nonWord->add_range('a', 'z');
  nonWord->add('_');
  nonWord->reverse();            // nonWordCC includes eolChar
  nonWordCC = nonWord->save();

  eol->add(eolChar);
  eolCC = eol->save();

  bothEol->add('\n');
  bothEol->add('\0');
  bothEolCC = bothEol->save();

  dot->reverse();
  dotCC = dot->save();
}

const CharClass *CharClass::boundary_nonb(char btype,
                                          bool second) {
  switch (btype) {
  case '^':                     // FIXME eolCC or bothEolCC?
    return (!second) ? bothEolCC : nullptr;
  case '$':                     // FIXME eolCC or bothEolCC?
    return (second) ? bothEolCC : nullptr;
  case '<':
    return (!second) ? nonWordCC : wordCC;
  case '>':
    return (second) ? nonWordCC : wordCC;
  }
  ASSERT(false);
  return nullptr;
}

const CharClass *CharClass::boundary(char btype,
                                     bool second,
                                     chartype otherch) {
  if (btype == 'b')
    return (wordCC->matches(otherch)) ? nonWordCC : wordCC;
  else if (btype == 'B')
    return (wordCC->matches(otherch)) ? wordCC : nonWordCC;
  else
    return boundary_nonb(btype, second);
}

const CharClass *CharClass::predef_class(char ctype) {
  if (ctype == 'w')
    return wordCC;
  else if (ctype == 'W')
    return nonWordCC;

  ERROR("Unknown predefined \\x character class!");
}

void CharClass::matching_chars(std::string & s) const {
  ostringstream ss;
  for (size_t i = 0; i < chars.size(); ++i)
    if (chars[i])
      ss << static_cast<chartype>(i);
  s = ss.str();
}

void CharClass::print(std::ostream & str) const {
  bool want;

  str << "[";

  if (chars[0]) {
    str << "^";
    want = false;
  } else {
    want = true;
  }

  // FIXME printing should handle ^ ] and - specially...
  for (size_t i = 0; i < chars.size(); i++) {
    if (chars[i] == want) {
      str << static_cast<chartype>(i);
      if (i < chars.size() - 2 &&
          chars[i+1] == want &&
          chars[i+2] == want) {
        while (i < chars.size() && chars[i] == want)
          i++;
        str << "-" << static_cast<chartype>(i-1);
      }
    }
  }

  str << "]";
}

const gap_size_t Gap::infinite;

void Gap::finishAdding() {
  vector<CCRange> ccrs;

  // Merge consecutive ranges with the same char classes (writing to ccrs)

  for (auto & ccr : ccrBegin) {
    if (ccrs.empty() || ccrs.back().cc != ccr.cc) {
      ccrs.push_back(ccr);
    } else {
      auto & last = ccrs.back();
      last.i = Gap::add(last.i, ccr.i);
      last.j = Gap::add(last.j, ccr.j);
    }
  }

  ccrBegin.clear();
  ccrEnd.clear();

  // Copy ccrs to ccrBegin, ccrEnd and ccrMid, updating i and j

  gap_size_t pos;
  vector<CCRange>::const_reverse_iterator riter;
  for (pos = 0, riter = ccrs.crbegin();
       riter != ccrs.crend() && riter->i == riter->j;
       ++riter) {
    auto & ccr = *riter;
    if (ccr.cc)
      ccrEnd.push_back({pos, ccr.i, ccr.cc});
    pos += ccr.i;
  }
  ccrEnd.shrink_to_fit();
  ccrMid.j = pos;

  if (riter == ccrs.crend()) {    // Only fixed-length gaps present
    ccrMid.i = 0;
    ccrMid.cc = nullptr;
    maxLength = minLength = ccrMid.j;
    return;
  }

  vector<CCRange>::const_iterator iter;
  for (pos = 0, iter = ccrs.cbegin();
       iter != ccrs.cend() && iter->i == iter->j;
       ++iter) {
    auto & ccr = *iter;
    if (ccr.cc)
      ccrBegin.push_back({pos, ccr.i, ccr.cc});
    pos += ccr.i;
  }
  ASSERT(iter != ccrs.cend());
  // Store these ccrs from end to beginning (the order used by matches())
  std::reverse(ccrBegin.begin(), ccrBegin.end());
  ccrBegin.shrink_to_fit();
  ccrMid.i = pos;

  // Now *iter and *riter are the first and last variable-length gaps

  if (iter == (riter + 1).base()) { // One var-len gap at *iter
    minLength = ccrMid.i + ccrMid.j + iter->i;
    maxLength = Gap::add(ccrMid.i + ccrMid.j, iter->j);
    ccrMid.cc = iter->cc;
  } else {                      // Multiple var-len gaps
    tooComplexMid = true;
    ccrMid.cc = nullptr;
    maxLength = minLength = ccrMid.i + ccrMid.j;
    // from *iter to before *riter
    for (; iter != riter.base(); ++iter) {
      auto & ccr = *iter;
      minLength += ccr.i;
      maxLength = Gap::add(maxLength, ccr.j);
    }
  }
}

std::ostream & Gap::formatLength(std::ostream & os, gap_size_t length) {
  if (length == Gap::infinite)
    os << "inf";
  else
    os << length;
  return os;
}

void Gap::print(std::ostream & str) const {
  str << "<gap";
  if (isTopLevel())
    str << "-";
  if (tooComplexMid)
    str << "~";
  if (needFallback)
    str << "F";
  str << getMinLength() << ",";
  Gap::formatLength(str, getMaxLength());
  if (ccrMid.cc)
    str << ":" << ccrMid.cc;
  else
    str << ";";
  str << ccrMid.i << "," << ccrMid.j;
  for (auto & ccr : ccrBegin)
    str << "/" << ccr.cc << ccr.i << "," << ccr.j;
  for (auto & ccr : ccrEnd)
    str << "\\" << ccr.cc << ccr.i << "," << ccr.j;
  str << ">";
}

const keywordid_t Pattern::begin_keyword_id;
const keywordid_t Pattern::end_keyword_id;

void Pattern::print(std::ostream & os) const {
  os << getPattern();
}

void Pattern::parse() {
  LOG("Parsing: " << pattern);
  PatternParser p(pattern);
  p.linkToPattern(*this);
}

keywordid_t Pattern::addKeyword(string const & keyword) {
  keywordid_t kwid = keywords.size();
  keywords.push_back(keyword);
  return kwid;
}

void Pattern::finalizeKeywords() {
  size_t n = keywords.size();
  if (n == 0) {
    WARN("Pattern has no keywords");
    throw invalid_pattern();
  }
  keywords.shrink_to_fit();
  gaps.resize(n);               // all elements are set later
  followSets.resize(n);
  preBoundaries.resize(n, nullptr);
  postBoundaries.resize(n, nullptr);
}

void Pattern::establishFallbackMatcher(bool alwaysNeeded) {
  alwaysFallback |= alwaysNeeded;
  if (fallbackMatcher != nullptr)
    return;                     // already done
  LOG("Establishing fallback matcher for pattern id#" << getId());
  fallbackMatcher = new regex_t;
  int ret = regcomp(fallbackMatcher,
                    pattern.c_str(),
                    REG_EXTENDED | REG_NOSUB);
  if (ret != 0) {
    delete fallbackMatcher;
    fallbackMatcher = nullptr;
    ASSERT(false);
  }
}

bool Pattern::useFallbackMatcher(const char *text) const {
  ASSERT(fallbackMatcher != nullptr);
  // FIXME use REG_NOTBOL and/or REG_NOTEOL?
  int ret = regexec(fallbackMatcher, text, 0, nullptr, 0);
  return (ret == 0);
}
