/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * A dynamic bitset. Wrapper for the STL bitset, allowing the bitset to grow.
 */

#include "util-BitSet.hpp"

using namespace std;
using namespace util;

ostream & util::operator<<(ostream & os, BitSet const & bs) {
  size_t blocks = bs.numBlocks();
  for (size_t i = 0; i < blocks; i++) {
    os << bs.pieces[i];
  }
  return os;
}

void BitSet::ensureCapacity(size_t ensureSize) {
  if (allocElements >= ensureSize)
    return;
  size_t tmp = allocElements * 2  + 1;
  if (ensureSize > tmp) {
    tmp = ensureSize;
  }
  size_t blocks = tmp / UTILS_BITSET_PIECE_SIZE;
  if (tmp % UTILS_BITSET_PIECE_SIZE != 0)
    blocks++;

  size_t newSize = blocks * UTILS_BITSET_PIECE_SIZE;
  ASSERT(newSize >= tmp);

  if (allocElements < newSize) {
    allocElements = newSize;
  }
  std::bitset < UTILS_BITSET_PIECE_SIZE > *oldPieces = pieces;
  pieces = new std::bitset<UTILS_BITSET_PIECE_SIZE>[blocks];
  size_t copyBlocks = numBlocks(nElements);
  for (size_t i = 0; i < copyBlocks; i++) {
    // Can't do memcpy, because the elements may be objects that have
    // constructors & destructors
    pieces[i] = oldPieces[i];
  }
  delete[] oldPieces;
}
