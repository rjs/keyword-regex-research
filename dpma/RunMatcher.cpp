/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Processes command-line options and starts the matcher
 */

#include "RunMatcher.hpp"
#include "Statistics.hpp"
#include "Pattern.hpp"
#include "Matcher.hpp"
#include "Reader.hpp"
#include "util-Timer.hpp"
#include "util-Log.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <cstring>
#include <vector>
#include <unistd.h>

int ::RunEnv::runFlags = 0;

using namespace std;

void Runner::usage(char const * name) {
  cout << "Usage:" << endl;
  cout << name << " <pattern> <textfile> " << endl;
  cout << "If <textfile> is not present, reads input from stdin" << endl;
  cout << "Options:" << endl;
  cout << "  -d          sets debug mode on" << endl;
  cout << "  -f file     reads patterns from file; leave out <pattern> in this case" << endl;
  cout << "  -m          outputs the match count only" << endl;
  cout << "  -l          outputs the first match locations for each pattern" << endl;
  cout << "  -e          output every match location with logger" << endl;
  cout << "  -s          stops on first match" << endl;
  cout << "  -L          line-based mode (patterns match a single line as in grep)" << endl;
  cout << "  -2          convert one-character keywords to gaps" << endl;
  cout << "  -K          transform some subpatterns into lists of keywords" << endl;
}

int Runner::matcherMain(int argc, char ** argv, string const & algorithmName,
                        MatcherFactory & factory) {
  bool debug = false;
  std::string textFile;
  std::string patternFile;
  std::string pattern;
  std::vector<string> args;
  int opt;

  while ((opt = getopt(argc, argv, "df:mlesL2K")) != -1) {
    switch (opt) {
    case 'd':
      debug = true;
      break;
    case 'f':
      patternFile = optarg;
      break;
    case 'm':
      ::RunEnv::setFlag(RUN_FLAG_OUTPUT_MATCH_COUNT);
      break;
    case 'l':
      ::RunEnv::setFlag(RUN_FLAG_OUTPUT_MATCH_POSITIONS);
      break;
    case 'e':
      ::RunEnv::setFlag(RUN_FLAG_OUTPUT_EVERY_MATCH);
      break;
    case 's':
      ::RunEnv::setFlag(RUN_FLAG_STOP_ON_FIRST);
      break;
    case 'L':
      ::RunEnv::setFlag(RUN_FLAG_LINE_MODE);
      break;
    case '2':
      ::RunEnv::setFlag(RUN_FLAG_TWO_CHAR_KEYWORDS);
      break;
    case 'K':
      ::RunEnv::setFlag(RUN_FLAG_MAKEKWS);
      break;
    default:
      usage(argv[0]);
      return 1;
    }
  }

  if (debug) {
    INFO("Setting debug mode on");
    util::Log::instance().setLogLevel(util::Log::Debug);
  }

  if (patternFile.empty()) {
    if (optind >= argc) {
      usage(argv[0]);
      return 1;
    }
    pattern = argv[optind++];
  }
  if (optind < argc) {
    textFile = argv[optind++];
  }
  if (!textFile.empty()) {
    Reader input(textFile);
    return runMatcher(pattern, patternFile, input, algorithmName,
                      factory);
  } else {
    Reader input;
    return runMatcher(pattern, patternFile, input, algorithmName,
                      factory);
  }
}

double Runner::getAveragePatternLength(vector<Pattern> const & patterns) {
  size_t total = 0;
  for (size_t i = 0; i < patterns.size(); i++) {
    Pattern const & p = patterns[i];
    for (size_t j = 0; j < p.getNumKeywords(); j++) {
      total += p.getKeyword(j).size();
    }
  }
  return (double) total / patterns.size();
}

size_t Runner::readPatterns(string const & pattern,
                            string const & patternfile,
                            MatcherFactory & factory,
                            vector<Pattern> & patterns) {
  size_t curP = 0;
  if (!pattern.empty()) {
    patterns.emplace_back(0, pattern);
    curP++;
  }

  ifstream file(patternfile.c_str());
  if (!file.good())
    return curP;
  string line;
  list<string> readLines;
  int linenum = 0;

  while (!file.eof()) {
    getline(file, line);
    ++linenum;
    // cout << "Read pattern: " << line << endl;
    if (!line.empty()) {
      try {
        patterns.emplace_back(linenum, line);
        LOG("Using pattern " << linenum << ": " << line);
      } catch (Pattern::invalid_pattern & e) {
        WARN("Removed invalid pattern " << linenum << ": " << line);
      }
      curP++;
    }
  }
  return curP;
}

int Runner::runMatcher(string const & pattern, string const & patternFile,
                       Reader & input, string const & algorithmName,
                       MatcherFactory & factory) {
  bool stopOnFirst = ::RunEnv::isFlagSet(RUN_FLAG_STOP_ON_FIRST);
  bool lineMode = ::RunEnv::isFlagSet(RUN_FLAG_LINE_MODE);
  util::Timer timerPre;
  timerPre.start();

  CharClass::init(lineMode ? '\n' : '\0');

  vector<Pattern> patterns;
  size_t nPats = readPatterns(pattern, patternFile, factory, patterns);
  if (nPats == 0) {
    cout << "No patterns found!" << endl;
    return 1;
  }

  Matcher * m = factory.createMatcher(patterns);
  m->setStopOnFirst(stopOnFirst);
  m->setLineMode(lineMode);

  timerPre.pause();

  util::Timer timer;
  timer.start();
  m->findMatches(&input);
  timer.pause();

  bool outputMatchCount = ::RunEnv::isFlagSet(RUN_FLAG_OUTPUT_MATCH_COUNT);
  bool outputMatchPositions = ::RunEnv::isFlagSet(RUN_FLAG_OUTPUT_MATCH_POSITIONS);
  bool showStats = !(outputMatchCount || outputMatchPositions);

  if (showStats) {
    string name = algorithmName;
    if (::RunEnv::isFlagSet(RUN_FLAG_TWO_CHAR_KEYWORDS))
      name = name + "-2charkw";
    if (lineMode)
      name = name + "-line";
    if (stopOnFirst)
      name = name + "-stop";

    m->reportMatches(cout,
                     timerPre.timeFromStart(), timer.timeFromStart(),
                     name);
  } else {
    if (outputMatchCount) {
      cout << m->getNMatches() << endl;
    }
    if (outputMatchPositions) {
      m->reportMatchPositions(cout);
    }
  }

  delete m;

  return 0;
}
