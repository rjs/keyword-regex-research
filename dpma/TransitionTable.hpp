/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Representation of transitions in the Aho-Corasick automatons
 */

#ifndef PM_TRANSITIONTABLE_HPP
#define PM_TRANSITIONTABLE_HPP

#include <map>
#include <bitset>

#define TRANSITIONTABLE_VALUE_TABLE

template<typename K, typename V>
class TransitionTable {
public:
  TransitionTable() : size(0), keys(nullptr), values(nullptr)
#ifdef TRANSITIONTABLE_VALUE_TABLE
                    , value_table(nullptr)
#endif
  { }

  ~TransitionTable() {
    if (keys != nullptr)
      delete[] keys;
    if (values != nullptr)
      delete[] values;
#ifdef TRANSITIONTABLE_VALUE_TABLE
    if (value_table != nullptr)
      delete[] value_table;
#endif
  }

  void set_all(std::map<K,V> const & m, V default_) {
    int i = 0;

    default_value = default_;

    if (keys != nullptr)
      delete[] keys;
    if (values != nullptr)
      delete[] values;

    size = m.size();
    keys = new K[size];
    values = new V[size];
    keybits.reset();

    for (auto it = m.cbegin();
         it != m.cend();
         i++, it++) {
      keys[i] = (*it).first;
      keybits.set((*it).first);
      values[i] = (*it).second;
    }

#ifdef TRANSITIONTABLE_VALUE_TABLE
    value_table = new V[1<<(8*sizeof(K))];
    update_value_table();
#endif
  }

  bool exists(K key) {
    int b = 0, e = size;

    if (!keybits[key])
      return false;

    while (b < e - 1) {
      int m = (b + e) / 2;
      if (key < keys[m])
        e = m;
      else
        b = m;
    }

    return (key == keys[b]);
  }

  V get(K key) {
#ifdef TRANSITIONTABLE_VALUE_TABLE
    return value_table[key];
#else
    int b = 0, e = size;

    if (!keybits[key])
      return default_value;

    while (b < e - 1) {
      int m = (b + e) / 2;
      if (key < keys[m])
        e = m;
      else
        b = m;
    }

    if (key == keys[b])
      return values[b];
    else
      return default_value;
#endif
  }

private:
#ifdef TRANSITIONTABLE_VALUE_TABLE
  void update_value_table() {
    size_t i;

    for (i = 0; i < 1<<(8*sizeof(K)); i++)
      value_table[i] = default_value;

    for (i = 0; i < size; i++)
      value_table[keys[i]] = values[i];
  }
#endif

  V default_value;
  size_t size;
  std::bitset<1<<(8*sizeof(K))> keybits;
  K *keys;
  V *values;
#ifdef TRANSITIONTABLE_VALUE_TABLE
  V *value_table;
#endif
};

#endif
