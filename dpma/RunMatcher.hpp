/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Processes command-line options and starts the matcher
 */

#ifndef PM_RUNMATCHER_HPP
#define PM_RUNMATCHER_HPP

#include "Statistics.hpp"
#include "Pattern.hpp"
#include <vector>
#include <string>

class Reader;

class MatcherFactory;

#define RUN_FLAG_STOP_ON_FIRST 1
#define RUN_FLAG_OUTPUT_MATCH_COUNT (1 << 1)
#define RUN_FLAG_OUTPUT_MATCH_POSITIONS (1 << 2)
#define RUN_FLAG_OUTPUT_EVERY_MATCH (1 << 3)
#define RUN_FLAG_LINE_MODE (1 << 4)
#define RUN_FLAG_TWO_CHAR_KEYWORDS (1 << 5)
#define RUN_FLAG_MAKEKWS (1 << 6)

#define MAX_LOG_MATCHES 10

class Runner {
public:

  /* Returns the system exit code (0 = success, <>0 = failure) */
  static int matcherMain(int argc, char ** argv,
                         std::string const & algorithmName,
                         MatcherFactory & factory);

  /* Returns the system exit code (0 = success, <>0 = failure) */
  static int runMatcher(std::string const & pattern,
                        std::string const & patternFile, Reader & input,
                        std::string const & algorithmName,
                        MatcherFactory & factory);

private:

  /* Prints usage */
  static void usage(char const * name);

  static double getAveragePatternLength(std::vector<Pattern> const & patterns);

  static size_t readPatterns(std::string const & pattern,
                             std::string const & patternfile,
                             MatcherFactory & factory,
                             std::vector<Pattern> & patterns);
};

class RunEnv {
public:
  static bool isFlagSet(int flag) {
    return (runFlags & flag) > 0;
  }
  static void setFlag(int flag) {
    runFlags |= flag;
  }

private:
  static int runFlags;
};

#endif // PM_RUNMATCHER_HPP
