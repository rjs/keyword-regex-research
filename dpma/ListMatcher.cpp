/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Our implementation of the matching algorithm described in P. Bille,
 * I. L. Gortz, H. Vildhoj and D. K. Wind, String Matching with
 * Variable Length Gaps, Theoretical Computer Science 443(2012):25-34.
 *
 * Extended to support multiple patterns and efficient line-based
 * matching. The matcher uses lists to store position ranges where the
 * next keyword is expected.
 */

#include "ListMatcher.hpp"
#include "Reader.hpp"
#include "Utils.hpp"
#include "util-Log.hpp"
#include "util-Collections.hpp"
#include <cstring>
#include <queue>
#include <algorithm>

using namespace std;

#define INITIAL_STATE 0
#define NONEXISTENT_STATE (static_cast<unsigned int>(-1))

ListPmaMatcher::ListPmaMatcher(std::vector<Pattern> & patterns) :
  Matcher(patterns), newState(INITIAL_STATE), nStates(0) {

  kwLists.resize(getNumPatterns());
  kwListAge.resize(getNumPatterns());
  vector<map<chartype, unsigned int> > pgotos;
  pgotos.push_back(map<chartype, unsigned int> ());
  for (unsigned int patId = 0; patId < patterns.size(); patId++) {
    Pattern const & pattern = patterns[patId];
    for (unsigned int kwId = 0; kwId < pattern.getNumKeywords(); kwId++) {
      addKeyword(pattern.getKeyword(kwId), pgotos, patId, kwId);
    }
    kwLists[patId].resize(pattern.getNumKeywords());
    kwListAge[patId].resize(pattern.getNumKeywords());
  }
  nStates = newState + 1;
  ASSERTEQUALS((size_t) nStates, pgotos.size());

  fails.resize(nStates);
  outputs.resize(nStates);

  constructFailStates(pgotos);

  addFailsToGotos(pgotos);
  addFailsToOutputs();

  gotos = new TransitionTable<chartype, unsigned int> [nStates];
  for (size_t i = 0; i < nStates; i++) {
    gotos[i].set_all(pgotos[i], INITIAL_STATE);
  }

  ASSERTEQUALS((unsigned int) newState + 1, nStates);
}

ListPmaMatcher::~ListPmaMatcher() {
  delete[] gotos;
}

void ListPmaMatcher::addKeyword(std::string const & keyword,
                                vector<map<chartype,unsigned int> > & pgotos,
                                unsigned int patId, unsigned int kwId) {
  if (keyword == "") {
    // There can be an empty keyword at the end
    keywordState[INITIAL_STATE] = true;
    outputs[INITIAL_STATE].push_back(OutputTuple(patId, kwId));
    // BUG("There must be no empty keywords in list matcher");
    return;
  }
  unsigned int state = 0;
  unsigned int j = 0;
  map<chartype, unsigned int>::const_iterator existing;
  while (j < keyword.length() &&
         (existing = pgotos[state].find(keyword[j])) != pgotos[state].cend()) {
    state = (*existing).second;
    j++;
  }
  for (unsigned int p = j; p < keyword.length(); p++) {
    // If there are multiple identical keywords, then this loop might
    // not be run
    newState++;
    pgotos.push_back(map<chartype, unsigned int> ());
    pgotos[state][keyword[p]] = newState;
    usedChars.insert(keyword[p]);
    state = newState;
    ASSERTEQUALS((size_t) newState + 1, pgotos.size());
  }
  if (!keywordState[state]) {
    // Mark state as a keyword state
    Matcher::stats.logOperation(Statistics::KeywordState);
    keywordState[state] = true;
  }
  if (outputs.size() < state + 1)
    outputs.resize(state + 1);
  outputs[state].push_back(OutputTuple(patId, kwId));
}

void ListPmaMatcher::constructFailStates(vector<map<chartype,unsigned int> > const & pgotos) {
  queue<unsigned int> queue;
  fails[INITIAL_STATE] = INITIAL_STATE;
  for (set<chartype>::const_iterator it = usedChars.cbegin();
       it != usedChars.cend();
       ++it) {
    chartype a = *it;
    if (pgotos[INITIAL_STATE].find(a) != pgotos[INITIAL_STATE].cend()) {
      unsigned int s = pgotos[INITIAL_STATE].find(a)->second;
      queue.push(s);
      fails[s] = INITIAL_STATE;
    }
  }
  while (!queue.empty()) {
    unsigned int r = queue.front();
    queue.pop();
    for (set<chartype>::const_iterator it = usedChars.cbegin();
         it != usedChars.cend();
         ++it) {
      chartype a = *it;
      if (pgotos[r].find(a) != pgotos[r].cend()) {
        unsigned int s = pgotos[r].find(a)->second;
        queue.push(s);
        unsigned int state = fails[r];
        while (state != INITIAL_STATE &&
               pgotos[state].find(a) == pgotos[state].cend()) {
          state = fails[state];
        }
        if (pgotos[state].find(a) == pgotos[state].cend())
          fails[s] = INITIAL_STATE;
        else
          fails[s] = pgotos[state].find(a)->second;
      }
    }
  }
}

void ListPmaMatcher::addFailsToGotos(
  vector<map<chartype,unsigned int> > & pgotos) {
  for (size_t i = 0; i < nStates; i++) {
    size_t s = i;
    do {
      s = fails[s];
      for (auto p : pgotos[s]) {
        chartype c = p.first;
        unsigned int v = p.second;
        if (pgotos[i].find(c) == pgotos[i].cend()) {
          pgotos[i][c] = v;
        }
      }
    } while (s != INITIAL_STATE);
  }
}

void ListPmaMatcher::addFailsToOutputs() {
  // FIXME Could be optimized: traverses the fail chains multiple
  // times and find is slow (use a set?)
  for (size_t i = 0; i < nStates; i++) {
    size_t s = i;
    do {
      s = fails[s];
      for (OutputTuple o : outputs[s]) {
        if (find(outputs[i].cbegin(), outputs[i].cend(), o) ==
            outputs[i].cend())
          outputs[i].push_back(o);
      }
    } while (s != INITIAL_STATE);
  }
}

void ListPmaMatcher::initializeOutput() {
  // does not clear kwLists (it is already empty)
  for (unsigned int i = 0; i < getNumPatterns(); i++) {
    Pattern const & pat = getPattern(i);
    Gap const & gap = pat.getGap(0);
    kwLists[i][0].push_front(MatchPos(gap.getMinLength(),
                                      gap.getMaxLength()));
    kwListAge[i].assign(pat.getNumKeywords(), 0);
  }
}

void ListPmaMatcher::findMatches(Reader *src_) {
  src = src_;

  initializeOutput();

  unsigned int state = INITIAL_STATE;
  chartype nextChar;

  charCount = 0;

  try {
    for (;;) {
      nextChar = static_cast<chartype>(src->readChar());
      LOG("Read " << static_cast<int>(nextChar) << " (" << nextChar << ")");
      // stats.logOperation(Statistics::ProcessChar);
      ASSERT(nextChar < 256);
      ASSERT(nextChar >= 0);
      charCount++;
      if (nextChar == '\n')
        lineBreak();

      state = gotos[state].get(nextChar);

      traverseOutputPath(state, charCount);
    }
  } catch (Reader::end_of_data &e) {
    ;
  } catch (stop_matching &e) {
    ;
  }

  src = nullptr;
}

void ListPmaMatcher::traverseOutputPath(unsigned int q, unsigned int charCount) {
  // Do something for this keyword (state q)
  // Process keywords that occur at this state
  for (OutputTuple o : outputs[q]) {
    stats.logOperation(Statistics::KeywordMatch);
    unsigned int patId = o.pattern();
    unsigned int kwId = o.keyword();

    Pattern const & pattern = getPattern(patId);

    // There is a match of keyword kwId at this position
    removeDeadRanges(patId, kwId);
    removeDeadRanges(patId, kwId + 1);

    if (kwId == 0 || firstRangeContains(patId, kwId, charCount
                                        - pattern.getKeyword(kwId).length())) {
      // A match that should be processed
      stats.logOperation(Statistics::PatternPrefixMatch);
      LOG("Match of pattern prefix; keyword " << kwId <<
          " of pat " << patId << " at char count " << charCount);
      if (kwId < pattern.getNumKeywords() - 1) {
        Gap const & gap = pattern.getGap(kwId + 1);
        MatchPos range(charCount + gap.getMinLength(),
                       Gap::add(charCount, gap.getMaxLength()));
        LOG("Appending range " << range << " for keyword " << (kwId + 1));
        appendRangeToEnd(patId, kwId + 1, range);
      } else {
        // Report a match
        reportMatch(charCount - 1, patId);
      }
    }
  }
}

void ListPmaMatcher::removeDeadRanges(unsigned int patId,
                                      unsigned int keywordId) {
  Pattern const & pat = getPattern(patId);
  if (keywordId >= pat.getNumKeywords())
    return;

  list<MatchPos> & list = kwLists[patId][keywordId];

  if (lineMode && kwListAge[patId][keywordId] < lineCount) {
    // List contents are from a previous line; clear them
    list.clear();

    if (keywordId == 0) {
      // Initialize as in initializeOutput
      Gap const & gap = pat.getGap(0);
      list.push_front(MatchPos(Gap::add(bolPos, gap.getMinLength()),
                               Gap::add(bolPos, gap.getMaxLength())));
    }

    kwListAge[patId][keywordId] = lineCount;
  }

  while (!list.empty()) {
    MatchPos & p = list.front();
    if (p.isDead(charCount, pat.getKeyword(keywordId).length())) {
      // Remove this
      list.pop_front();
    } else {
      // First range is not dead, so we can stop
      break;
    }
  }
}
