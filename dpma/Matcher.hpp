/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Common superclass for all the matchers. Deals with reporting
 * matches and (for dpmare) running the fallback matcher.
 */

#ifndef PM_MATCHER_HPP
#define PM_MATCHER_HPP

#include "RunMatcher.hpp"
#include "Pattern.hpp"
#include "Utils.hpp"
#include "Statistics.hpp"
#include <queue>
#include <string>
#include <vector>
#include <exception>

typedef void (* MatchPositionCallback)(int patId, int position);

class Reader;

class Matcher {

public:
  class stop_matching: public std::exception {
  public:
    explicit stop_matching() : std::exception() { }
  };

  struct PosPatid {
    class SmallerPosCmp {
    public:
      bool operator() (const PosPatid & lhs, const PosPatid & rhs) {
        return lhs.pos < rhs.pos;
      }
    };
    PosPatid(size_t pos_, size_t patId_)
      : pos(pos_), patId(patId_) { }
    ~PosPatid() { }
    size_t pos, patId;
  };

  static const size_t maxPostFallback = 1000;

  Matcher(std::vector<Pattern> & patterns_) :
    patterns(patterns_),
    reportEveryMatch(::RunEnv::isFlagSet(RUN_FLAG_OUTPUT_EVERY_MATCH)),
    currentLineMatched(false),
    matchedLines(0), remainingPatterns(patterns_.size()),
    matches(), firstMatches(), callback(0), fallback_queue(),
    stopOnFirst(false), lineMode(false), src(0),
    matchBegin(), nMatches(0), stats(),
    charCount(0), lineCount(0), bolPos(0) {
    matches.resize(patterns.size());
    firstMatches.resize(patterns.size());
    matchBegin.resize(patterns.size());
  }
  virtual ~Matcher() {
  }

  virtual void findMatches(Reader *src_) = 0;

  virtual Pattern const & getPattern(unsigned int index) const {
    return patterns[index];
  }

  Pattern & getPattern(unsigned int index) {
    return patterns[index];
  }

  size_t getNumPatterns() const {
    return patterns.size();
  }

  void setStopOnFirst(bool stopOnFirst_) {
    stopOnFirst = stopOnFirst_;
  }

  // FIXME support patterns that use multiline matching? (perl //m)
  // FIXME lineMode = false is not egrep -z ('\0' doesn't reset matcher)
  void setLineMode(bool lineMode_) {
    lineMode = lineMode_;
  }

  void setCallback(MatchPositionCallback callback_) {
    callback = callback_;
  }

  void lineBreak() {
    lineCount++;
    bolPos = charCount;
    currentLineMatched = false;
  }

  virtual void reportMatch(size_t matchPos, size_t patId) {
    ASSERT(matchPos >= bolPos); // otherwise lineCount and
                                // currentLineMatched may be wrong
    Pattern & pat = getPattern(patId);
    if (stopOnFirst) {
      if (!pat.isFound()) {
        ASSERT(remainingPatterns > 0);
        remainingPatterns--;
      } else {
        LOG("Pattern " << patId << " has already been found, skipping match");
        return;
      }
    }
    if (pat.isFound() && pat.getLastFoundPos() == matchPos) {
      LOG("Match already reported at " << matchPos << ", discarding match");
      return;
    }
    if (reportEveryMatch) {
      INFO("Match of pattern " << patId << ": " << getPattern(patId) <<
           " at pos " << matchPos << " line " << lineCount);
    } else {
      LOG("Match of pattern " << patId << ": " << getPattern(patId) <<
          " at pos " << matchPos << " line " << lineCount);
    }
    pat.setFoundPos(matchPos);

    if (!currentLineMatched) {
      currentLineMatched = true;
      matchedLines++;
    }

    stats.logOperation(Statistics::PatternMatch);
    if (callback != 0)
      callback(patId, matchPos);
    if (matches[patId] == 0)
      firstMatches[patId] = matchPos;
    matches[patId]++;
    nMatches++;

    if (stopOnFirst && remainingPatterns == 0)
      throw stop_matching();
  }

  unsigned int reportMatches(std::ostream & str,
                             double preElapsed, double elapsed,
                             std::string const & algorithm) {
    unsigned int nPatMatches = 0;
    unsigned int nnMatches = 0;

    std::stringstream s;
    for (unsigned int i = 0; i < patterns.size(); i++) {
      if (matches[i] > 0) {
        nnMatches += matches[i];
        nPatMatches++;
      }
    }
    ASSERT(nnMatches == nMatches);
    str << "<ALGORITHM>" << algorithm << "</ALGORITHM>" << std::endl
        << "<NUM_PATTERNS>" << patterns.size() << "</NUM_PATTERNS>" << std::endl
        << "<PRE_ELAPSED> " << preElapsed << "</PRE_ELAPSED>" << std::endl
        << "<ELAPSED>" << elapsed << "</ELAPSED>" << std::endl
        << "<MATCHES>"  << nnMatches << "</MATCHES>" << std::endl
        << "<MATCHED_LINES>"  << matchedLines << "</MATCHED_LINES>" << std::endl;

    for (int i = Statistics::NoOp + 1; i != Statistics::NumOps; ++i) {
      Statistics::Operation op = static_cast<Statistics::Operation> (i);
      if (stats.getNumOperations(op) != 0) {
        str << "<STATISTICS_" << Statistics::getOperationName(op) << ">"
            << stats.getNumOperations(op)
            << "</STATISTICS_" << Statistics::getOperationName(op) << ">"
            << std::endl;
      }
    }

    return nnMatches;
  }

  void reportMatchPositions(std::ostream & str) {
    for (unsigned int i = 0; i < patterns.size(); i++) {
      if (matches[i] > 0) {
        str << i << ": " << matches[i] << " (" << firstMatches[i]
            << ")" << std::endl;
      }
    }
  }

  size_t getNumProcessedChars() const {
    return charCount;
  }

  unsigned int getNMatches() const {
    return nMatches;
  }

  unsigned int getMatchedLines() const {
    return matchedLines;
  }

  void setupOrExecuteFallback(size_t patId) {
    gap_size_t fallbackAmount = getPattern(patId).getPostFallbackAmount();
    if (fallbackAmount == 0) {
      executeFallback(patId, charCount);
    } else if (fallbackAmount == Gap::infinite ||
               fallbackAmount > maxPostFallback) {
      setupFallbackAt(patId, charCount + maxPostFallback);
    } else {
      setupFallbackAt(patId, charCount + fallbackAmount);
    }
  }

  void setupFallbackAt(size_t patId, size_t pos) {
    fallback_queue.emplace(pos, patId);
  }

  void runFallbacks(bool all) {
    while (!fallback_queue.empty()) {
      const PosPatid & p = fallback_queue.top();
      size_t matchPos = p.pos;
      size_t patId = p.patId;
      if (matchPos > charCount) {
        if (!all)
          break;
        matchPos = charCount;
      }
      executeFallback(patId, matchPos);
      fallback_queue.pop();
    }
  }

  void executeFallback(size_t patId, size_t matchPos) {
    const Pattern & pat = getPattern(patId);
    std::string t = src->prevChars_str(charCount - matchBegin[patId],
                                       charCount - matchPos);
    stats.logOperation(Statistics::UseFallbackMatcher);
    if (!pat.useFallbackMatcher(t)) {
      LOG("Discarding pattern match because fallback does not match");
      stats.logOperation(Statistics::FallbackNoMatch);
    } else {
      reportMatch(matchPos - 1, patId);
    }
  }

private:
  std::vector<Pattern> & patterns;
  bool const reportEveryMatch;
  bool currentLineMatched;
  unsigned int matchedLines;
  size_t remainingPatterns;
  std::vector<unsigned int> matches, firstMatches;
  MatchPositionCallback callback;
  std::priority_queue<PosPatid,
                      std::vector<PosPatid>,
                      PosPatid::SmallerPosCmp> fallback_queue;

protected:
  bool stopOnFirst;
  bool lineMode;
  Reader *src;
  std::vector<size_t> matchBegin;
  unsigned int nMatches;
  Statistics stats;
  size_t charCount;
  size_t lineCount;
  size_t bolPos;
};

class MatcherFactory {
public:
  MatcherFactory() { }
  virtual ~MatcherFactory() { }
  virtual Matcher * createMatcher(std::vector<Pattern> & patterns) = 0;
};

#endif // PM_MATCHER_HPP
