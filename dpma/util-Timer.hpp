/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Provides a timer that calculates time from its initialization and
 * last call to it
 */

#if !defined(UTIL_TIMER_HPP)
#define UTIL_TIMER_HPP

#if defined(WIN32)
#include <windows.h>
#else // defined(WIN32)
#include <sys/time.h>
#endif // defined(WIN32)
namespace util {

  class Timer {
  public:
    Timer();
    ~Timer();

    /*
     * Starts the timer
     */
    void start();

    /*
     * Pauses the timer
     */
    void pause();

    /*
     * Returns the time from the start of the timer without including pauses.
     */
    double timeFromStart();

    /*
     * Returns the time from the last (absolute) invocation of this method.
     * Pauses are not handled so they do not affect the outcome of this
     * method.
     */
    double timeFromLast();

  private:
    bool paused;
    double accumulated;

#if defined(WIN32)
    DWORD startTime;
    DWORD lastTime;
#else // defined(WIN32)
    struct timeval startTime;
    struct timeval lastTime;
#endif // defined(WIN32)
  };

}

#endif // !defined(UTIL_TIMER_HPP)
