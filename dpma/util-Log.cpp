/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Basic logging functions and assertions. The log level can be set at
 * run-time.
 */

#include "util-Log.hpp"
#include <sstream>

using namespace std;
using namespace util;

static stringstream nullStream;

Log & Log::instance() {
  static Log _the_log;
  return _the_log;
}

ostream & Log::getStream(Level level, int line, char const * file,
                         char const * levelName) {
  ostream * stream = &clog;
  if (level >= Warn) {
    stream = &cerr;
  }
  if (logLevel <= level) {
    *stream << levelName << " " << file << ":" << line << "  ";
    return *stream;
  } else {
    nullStream.flush();
    return nullStream;
  }
}

ostream & Log::debug(int line, char const * file) {
  Log & log = Log::instance();
  return log.getStream(Debug, line, file, "DEBUG");
}

ostream & Log::info(int line, char const * file) {
  Log & log = Log::instance();
  return log.getStream(Info, line, file, "INFO ");
}

ostream & Log::warn(int line, char const * file) {
  Log & log = Log::instance();
  return log.getStream(Warn, line, file, "WARN ");
}

ostream & Log::error(int line, char const * file) {
  Log & log = Log::instance();
  return log.getStream(Error, line, file, "ERROR");
}

ostream & Log::bug(int line, char const * file) {
  Log & log = Log::instance();
  return log.getStream(Bug, line, file, "BUG  ");
}
