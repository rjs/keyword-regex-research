/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Our implementation of the matching algorithm described in P. Bille,
 * I. L. Gortz, H. Vildhoj and D. K. Wind, String Matching with
 * Variable Length Gaps, Theoretical Computer Science 443(2012):25-34.
 *
 * Extended to support multiple patterns and efficient line-based
 * matching. The matcher uses lists to store position ranges where the
 * next keyword is expected.
 */

#ifndef PM_LISTMATCHER_HPP
#define PM_LISTMATCHER_HPP

#include "Matcher.hpp"
#include "TransitionTable.hpp"
#include "util-Log.hpp"
#include "util-BitSet.hpp"
#include <map>
#include <list>
#include <utility>
#include <vector>

class ListPmaMatcher: public Matcher {
public:

  typedef std::pair<unsigned int, unsigned int> MatchPosType;

  class OutputTuple: public MatchPosType {

  public:
    OutputTuple() :
      MatchPosType(0, 0) {
    }
    OutputTuple(unsigned int pattern, unsigned int keyword) :
      MatchPosType(pattern, keyword) {
    }

    unsigned int pattern() const {
      return first;
    }

    unsigned int keyword() const {
      return second;
    }
  };

  class MatchPos: public MatchPosType {
  public:
    MatchPos() :
      MatchPosType(0, 0) {
    }
    MatchPos(unsigned int start, unsigned int end) :
      MatchPosType(start, end) {
    }

    unsigned int start() const {
      return first;
    }
    unsigned int end() const {
      return second;
    }

    bool isDead(unsigned int charPos, unsigned int kwLen) const {
      return (charPos - kwLen) > second;
    }

    bool contains(unsigned int position) const {
      return position >= first && position <= second;
    }

    bool overlaps(MatchPos const & other) const {
      return first <= other.second && second >= other.first;
    }

    MatchPos extend(MatchPos const & other) const {
      return MatchPos(MIN(first, other.first), MAX(second, other.second));
    }

  };

  ListPmaMatcher(std::vector<Pattern> & patterns);
  virtual ~ListPmaMatcher();

  virtual void findMatches(Reader *src_);

private:
  void traverseOutputPath(unsigned int q, unsigned int charCount);

  void countMaxDistAndKWLen();
  void addKeyword(std::string const & keyword,
                  std::vector<std::map<chartype,unsigned int> > & pgotos,
                  unsigned int patId, unsigned int kwId);
  void constructFailStates(
    std::vector<std::map<chartype,unsigned int> > const & pgotos);
  void addFailsToGotos(std::vector<std::map<chartype,unsigned int> > & pgotos);
  void addFailsToOutputs();

  void initializeOutput();
  void removeDeadRanges(unsigned int patId, unsigned int keywordId);
  void appendRangeToEnd(unsigned int patId, unsigned int keywordId,
                        MatchPos range) {
    std::list<MatchPos> & list = kwLists[patId][keywordId];
    while (!list.empty() && list.back().overlaps(range)) {
      range = range.extend(list.back());
      list.pop_back();
    }
    list.push_back(range);
  }

  bool firstRangeContains(unsigned int patId, unsigned int keywordId,
                          unsigned int position) {
    std::list<MatchPos> & list = kwLists[patId][keywordId];
    if (list.empty())
      return false;
    return list.front().contains(position);
  }

private:
  unsigned int newState;
  unsigned int nStates;
  util::BitSet keywordState;
  std::vector<std::vector<OutputTuple> > outputs;
  std::vector<std::vector<std::list<MatchPos> > > kwLists;
  std::vector<std::vector<size_t > > kwListAge; // line number
  TransitionTable<chartype, unsigned int> *gotos;
  std::set<chartype> usedChars;
  std::vector<unsigned int> fails;
};

class ListPmaMatcherFactory: public MatcherFactory {
public:
  ListPmaMatcherFactory() { }

  Matcher * createMatcher(std::vector<Pattern> & patterns) {
    return new ListPmaMatcher(patterns);
  }
};

#endif // PM_LISTMATCHER_HPP
