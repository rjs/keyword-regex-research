/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Reading the input text efficiently. Reads text into a ring buffer
 * using the read() system call. Most of the methods are for accessing
 * the ring buffer nicely.
 */

#ifndef PM_READER_HPP
#define PM_READER_HPP

#include <exception>
#include <stdexcept>
#include <climits>
#include <cstring>
#include <unistd.h>

class ReadString {
public:
  class read_off_end: public std::exception {
  public:
    explicit read_off_end() : std::exception() { }
  };

  // buf1 is before buf2 in the string (either one can be empty)
  ReadString(const unsigned char *buf1_, size_t len1_,
             const unsigned char *buf2_, size_t len2_)
    : buf1(buf1_), buf2(buf2_), len1(len1_), len2(len2_) { }
  ReadString(const unsigned char *buf_, size_t len_)
    : ReadString(buf_, len_, nullptr, 0) { }

  size_t length() const {
    return len1 + len2;
  }

  unsigned char at(size_t pos) const {
    if (pos >= len1 + len2)
      throw read_off_end();
    if (pos < len1)
      return buf1[pos];
    else
      return buf2[pos - len1];
  }

  // pos from end of string
  unsigned char rat(size_t pos) const {
    if (pos >= len1 + len2)
      throw read_off_end();
    if (pos < len2)
      return buf2[len2 - pos - 1];
    else
      return buf1[len1 + len2 - pos - 1];
  }

private:
  const unsigned char *buf1, *buf2;
  size_t len1, len2;
};

class Reader {
public:
  // bufsize - readsize (32 KB) is the maximum n for prevChars
  static constexpr size_t readsize =
    (SSIZE_MAX > 32*1024UL) ? 32*1024UL : SSIZE_MAX;
  static constexpr size_t bufsize = 256*1024UL;
  // minimum amount of readahead to keep for aheadChar_or
  static constexpr size_t minreadahead = 32;

  class end_of_data: public std::exception {
  public:
    explicit end_of_data() : std::exception() { }
  };

  Reader() : fd(STDIN_FILENO), pos(buf), end(buf), bufend(buf),
             needClose(false), readEof(false) { }
  explicit Reader(int fd_) : Reader() { fd = fd_; }
  explicit Reader(const char *fileName);
  explicit Reader(std::string const & fileName) : Reader(fileName.c_str()) { }

  ~Reader() {
    if (needClose)
      close(fd);
  }

  unsigned char readChar() {
    size_t remaining = static_cast<size_t>(end - pos);
    if (remaining < minreadahead && (!readEof || remaining == 0)) {
      if (readEof && remaining == 0)
        throw end_of_data();

      if (end > buf + bufsize - readsize) {
        memmove(buf, pos, remaining);
        bufend = pos;
        pos = buf;
        end = buf + remaining;
      }

      int i = read(fd, end, readsize);
      if (i <= 0) {
        if (i == 0) {
          readEof = true;
          if (remaining == 0)
            throw end_of_data();
        } else {
          throw std::runtime_error(std::string("read: ") + strerror(errno));
        }
      }

      end += i;
    }

    return *pos++;
  }

  // i=1 for last character
  unsigned char prevChar_or(size_t i, unsigned char fallback) const;
  // i=0 to peek at next character
  unsigned char aheadChar_or(size_t i, unsigned char fallback) const;

  // Returns characters [current pos - n .. current pos - m)
  // n=1, m=0 => the last character read
  std::string prevChars_str(size_t n, size_t m = 0) const;
  // Returns characters [current pos - n .. current pos - m)
  ReadString prevChars(size_t n, size_t m = 0) const;

private:
  int fd;
  unsigned char *pos, *end, *bufend;
  unsigned char buf[bufsize];
  bool needClose, readEof;
};

#endif // PM_READER_HPP
