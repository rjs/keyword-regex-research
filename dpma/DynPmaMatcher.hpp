/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * The main part of the matching algorithm described in "Online
 * Dictionary Matching with Variable-Length Gaps" (SEA 2011).
 */

#ifndef PM_DYNPMAMATCHER_HPP
#define PM_DYNPMAMATCHER_HPP

#include "Matcher.hpp"
#include "TransitionTable.hpp"
#include "util-Collections.hpp"
#include "util-BitSet.hpp"
#include "util-Log.hpp"
#include <limits>
#include <map>
#include <set>
#include <list>
#include <string>
#include <vector>
#include <iostream>
#include <utility>

#define DPMA_ENDPOS_NOT_SET 0

class DynamicPmaMatcher: public Matcher {
public:
  struct PendingTuple: public virtual Printable {
    size_t _b;
    unsigned int _pattern;
    unsigned int _keyword;
    size_t _e;

    static const size_t no_end = std::numeric_limits<size_t>::max();

    PendingTuple() : PendingTuple(0, 0, 0, 0) { }
    PendingTuple(size_t b_, unsigned int pattern_,
                 unsigned int keyword_, size_t e_) :
      _b(b_), _pattern(pattern_), _keyword(keyword_), _e(e_) { }
    ~PendingTuple() { }
    size_t b() const { return _b; }
    unsigned int pattern() const { return _pattern; }
    unsigned int keyword() const { return _keyword; }
    size_t e() const { return _e; }
    virtual void print(std::ostream & os) const {
      os << '(' << _b << ','
         << _pattern << ',' << _keyword << ',' << _e << ')';
    }
  };

  class OutputTuple: public std::pair<unsigned int, unsigned int> {

  public:
    OutputTuple() :
      std::pair<unsigned int, unsigned int>(0, 0) {
    }
    OutputTuple(unsigned int pattern, unsigned int keyword) :
      std::pair<unsigned int, unsigned int>(pattern, keyword) {
    }

    unsigned int pattern() const {
      return first;
    }
    unsigned int keyword() const {
      return second;
    }
  };

  /*
   * An alternative for some data structures: store pending and
   * current output tuples in std::sets.
   *
   * typedef std::set<PendingTuple> PendingSetType;
   * typedef std::set<OutputTuple> OutputSetType;
   * #define OP_INSERT(set, value) (set).insert(value);
   * #define OP_ERASE(set, iterator) (set).erase(iterator++);
   * #define OP_FOREACH(iterator)
   * #define OP_FOREND(iterator) iterator++
   *
   * The uncommented choices below store pending and current output
   * tuples in linked lists (std::list).
   */
  typedef std::list<PendingTuple> PendingSetType;
  typedef std::list<OutputTuple> OutputSetType;
#define OP_INSERT(set, value) (set).push_back(value);
#define OP_ERASE(set, iterator) iterator = (set).erase(iterator);
#define OP_FOREACH(iterator)
#define OP_FOREND(iterator) iterator++

public:
  DynamicPmaMatcher(std::vector<Pattern> & patterns);
  virtual ~DynamicPmaMatcher();

  virtual void findMatches(Reader *src_);

private:
  void distributeOutput();
  void distributeOutputTuple(PendingTuple const & po) {
    Matcher::stats.logOperation(Statistics::DistributeOutput);
    if (Matcher::stopOnFirst) {
      Pattern & pat = Matcher::getPattern(po.pattern());
      // If we only find the first matches and this pattern has been
      // found, do not process this tuple
      if (pat.isFound())
        return;
    }
    unsigned int q = kwState[po.pattern()][po.keyword()];
    LOG("Distributing " << po << " to state " << q);
    ASSERT(q < nStates);
    checkAge_state(q);
    checkAge_patkw(po.pattern(), po.keyword());
    size_t & maxBPos = maxBeginPositions[po.pattern()][po.keyword()];
    size_t & maxEPos = maxEndingPositions[po.pattern()][po.keyword()];
    if (maxEPos == DPMA_ENDPOS_NOT_SET) {
      // There is no current output tuple for this keyword
      OP_INSERT(currentOutputs[q],
                OutputTuple(po.pattern(), po.keyword()));
    } else {
      // Only update the begin and ending positions of this keyword
      ASSERT(maxBPos <= po.b());
      ASSERT(maxEPos <= po.e());
    }
    maxBPos = po.b();
    maxEPos = po.e();
    LOG("State " << q << " is " << currentOutputs[q]);
  }
  void traverseOutputPath(unsigned int q);
  unsigned int outputFail(unsigned int q);

  void countMaxDistAndKWLen();
  void addKeyword(std::string const & keyword, std::vector<std::map<chartype,
                  unsigned int> > & pgotos, size_t patId, size_t kwId);
  void constructFailStates(std::vector<std::map<chartype,unsigned int> > const & pgotos);
  void constructStaticOutputFails();
  unsigned int findStaticOutputFail(unsigned int state);
  void addFailsToGotos(std::vector<std::map<chartype,unsigned int> > & pgotos);

  void resetPendingOutput(size_t beginPos);
  void initializeOutput();

  void checkAge_state(unsigned int q) {
    if (Matcher::lineMode && stateAge[q] < Matcher::lineCount) {
      stateAge[q] = Matcher::lineCount;
      currentOutputs[q].clear();
    }
  }

  void checkAge_patkw(size_t patId, size_t kwId) {
    if (Matcher::lineMode && patkwAge[patId][kwId] < Matcher::lineCount) {
      patkwAge[patId][kwId] = Matcher::lineCount;
      // maxBeginPositions is not read if maxEndingPositions = not set
      maxEndingPositions[patId][kwId] = DPMA_ENDPOS_NOT_SET;
      if (kwId == 0) {
        maxKWFound[patId] = -1;
        needFallback[patId] = Matcher::getPattern(patId).isAlwaysFallback();
      }
    }
  }

  void checkAge_pat(size_t patId) {
    checkAge_patkw(patId, 0);
  }

private:

  unsigned int maxDist;
  unsigned int maxKWLen;
  unsigned int newState;
  size_t nStates;
  util::BitSet keywordState;
  TransitionTable<chartype, unsigned int> *gotos;
  std::set<chartype> usedChars;
  unsigned int *fails;
  unsigned int *outputFails;
  PendingSetType *pendingOutput;
  OutputSetType *currentOutputs;
  std::vector<std::vector<size_t> > maxBeginPositions;
  std::vector<std::vector<size_t> > maxEndingPositions;
  int *maxKWFound;
  bool *needFallback;
  unsigned int **kwState;

  size_t *stateAge; // lineCount for currentOutputs
  // lineCount for maxBeginPositions, maxEndingPositions, maxKWFound (kwId==0)
  std::vector<std::vector<size_t> > patkwAge;
};

class DynamicPmaMatcherFactory: public MatcherFactory {
public:
  DynamicPmaMatcherFactory() { }
  Matcher * createMatcher(std::vector<Pattern> & patterns) {
    return new DynamicPmaMatcher(patterns);
  }
};

#endif // PM_DYNPMAMATCHER_HPP
