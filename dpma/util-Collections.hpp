/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Printing functions for some STL collection types
 */

#if !defined(UTIL_COLLECTIONS_HPP)
#define UTIL_COLLECTIONS_HPP

#include <set>
#include <vector>
#include <list>
#include <deque>
#include <map>

template<typename T>
std::ostream & operator<<(std::ostream & os, std::vector<T> const & ar) {
  os << "[";
  for (size_t i = 0; i < ar.size(); i++) {
    if (i != 0)
      os << ", ";
    os << ar[i];
  }
  os << "]";
  return os;
}

template<typename T>
std::ostream & operator<<(std::ostream & os, std::deque<T> const & ar) {
  os << "[";
  bool first = true;
  for (typename std::deque<T>::const_iterator it = ar.begin();
       it != ar.end();
       ++it) {
    if (!first)
      os << ", ";
    os << *it;
    first = false;
  }
  os << "]";
  return os;
}

template<typename T>
std::ostream & operator<<(std::ostream & os, std::list<T> const & li) {
  os << "[";
  bool first = true;
  for (typename std::list<T>::const_iterator it = li.begin();
       it != li.end();
       ++it) {
    if (!first)
      os << ", ";
    os << *it;
    first = false;
  }
  os << "]";
  return os;
}

template<typename T>
std::ostream & operator<<(std::ostream & os, std::set<T> const & st) {
  os << "{";
  bool first = true;
  for (typename std::set<T>::const_iterator it = st.begin();
       it != st.end();
       ++it) {
    if (!first)
      os << ", ";
    os << *it;
    first = false;
  }
  os << "}";
  return os;
}

template<typename T1, typename T2>
std::ostream & operator<<(std::ostream & os, std::pair<T1, T2> const & pair) {
  os << "<" << pair.first << ", " << pair.second << ">";
  return os;
}

#endif // !defined(UTIL_COLLECTIONS_HPP)
