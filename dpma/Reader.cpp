/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Reading the input text efficiently. Reads text into a ring buffer
 * using the read() system call. Most of the methods are for accessing
 * the ring buffer nicely.
 */

#include <stdexcept>
#include <climits>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include "Reader.hpp"
#include "util-Log.hpp"

using std::string;
using std::runtime_error;

Reader::Reader(const char *filename) : Reader() {
  fd = open(filename, O_RDONLY);
  if (fd < 0)
    throw runtime_error(string("open: ") + strerror(errno));
  needClose = true;
}

unsigned char Reader::prevChar_or(size_t i, unsigned char fallback) const {
  if (i <= static_cast<size_t>(pos - buf))
    return *(pos - i);
  if (bufend > end &&
      i <= static_cast<size_t>((pos - buf) + (bufend - end)))
    return *(bufend - (i - (pos - buf)));
  return fallback;
}

unsigned char Reader::aheadChar_or(size_t i, unsigned char fallback) const {
  ASSERT(i < minreadahead);
  if (i >= static_cast<size_t>(end - pos))
    return fallback;
  return *(pos + i);
}

string Reader::prevChars_str(size_t n, size_t m) const {
  ASSERT(n >= m);
  if (n <= static_cast<size_t>(pos - buf))
    return string(reinterpret_cast<const char *>(pos - n), n - m);
  if (bufend > end &&
      n <= static_cast<size_t>((pos - buf) + (bufend - end))) {
    size_t nn = n - (pos - buf);
    if (n - m <= nn) {
      return string(reinterpret_cast<const char *>(bufend - nn), n - m);
    } else {
      return string(reinterpret_cast<const char *>(bufend - nn), nn).
        append(reinterpret_cast<const char *>(buf), pos - buf - m);
    }
  }
  throw runtime_error("looking too far in history");
}

ReadString Reader::prevChars(size_t n, size_t m) const {
  ASSERT(n >= m);
  if (n <= static_cast<size_t>(pos - buf))
    return ReadString(pos - n, n - m);
  if (bufend > end &&
      n <= static_cast<size_t>((pos - buf) + (bufend - end))) {
    size_t nn = n - (pos - buf);
    if (n - m <= nn)
      return ReadString(bufend - nn, n - m);
    else
      return ReadString(bufend - nn, nn, buf, pos - buf - m);
  }
  // n is more than we have in the buffer, but give as much as there is
  return ReadString(buf, pos - buf, end, bufend - end);
}
