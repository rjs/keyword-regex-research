/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Basic logging functions and assertions. The log level can be set at
 * run-time.
 */

#if !defined(UTIL_LOG_HPP)
#define UTIL_LOG_HPP

#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>

#ifdef ERROR
#undef ERROR
#endif

#ifdef assert
#undef assert
#endif // assert
#ifdef ASSERT
#undef ASSERT
#endif // ASSERT

#define LOGATLEVEL(level, func, x) { if (util::Log::instance().logAtLevel(level)) { func(__LINE__, __FILE__) << x << std::endl << std::flush; } }

#ifdef NDEBUG
#define LOG(x) ;
#define INFO(x) ;
#else
#define LOG(x) LOGATLEVEL(util::Log::Debug, util::Log::debug, x)
#define INFO(x) LOGATLEVEL(util::Log::Info, util::Log::info, x)
#endif

#define WARN(x) LOGATLEVEL(util::Log::Warn, util::Log::warn, x)

#define ERROR(x) {                                                      \
    std::stringstream _e_msg;                                           \
    _e_msg << x;                                                        \
    util::Log::error(__LINE__, __FILE__) << _e_msg.str() << std::endl;  \
    throw std::runtime_error(_e_msg.str());                             \
  }
#define BUG(x) {                                                        \
    std::stringstream _b_msg;                                           \
    _b_msg << x;                                                        \
    util::Log::bug(__LINE__, __FILE__) << _b_msg.str() << std::endl;    \
    throw std::runtime_error(_b_msg.str());                             \
  }
#define BUG_LF(line, file, x) {                                 \
    std::stringstream _b_msg;                                   \
    _b_msg << x;                                                \
    util::Log::bug(line, file) << _b_msg.str() << std::endl;    \
    throw std::runtime_error(_b_msg.str());                     \
  }

namespace util {
  class Log {
  public:
    static Log & instance();

    typedef enum {
      Debug, Info, Warn, Error, Bug
    } Level;

    static std::ostream & debug(int line, char const * file);
    static std::ostream & info(int line, char const * file);
    static std::ostream & warn(int line, char const * file);
    static std::ostream & error(int line, char const * file);
    static std::ostream & bug(int line, char const * file);

    void setLogLevel(Level level) {
      logLevel = level;
    }
    Level getLogLevel() {
      return logLevel;
    }
    bool logAtLevel(Level level) {
      return logLevel <= level;
    }

    static void assert(int line, char const * file, bool condition,
                       char const * condStr) {
      if (!condition) {
        BUG_LF(line, file, "Assertion failed: " << condStr);
      }
    }

    template<typename T>
    static void assertEquals(int line, char const * file, T const & expected,
                             T const & actual, char const * expectedName,
                             char const * actualName) {
      if (!(expected == actual)) {
        BUG_LF(line, file,
               "Assertion failed: " << expectedName << " != " << actualName
               << " (expected " << expected << ", got " << actual << ")");
      }
    }

  private:
    Log() :
      logLevel(Info) {
    }
    std::ostream & getStream(Level level, int line, char const * file,
                             char const * levelName);

    Level logLevel;
  };
}

#ifdef NDEBUG
#define ASSERT(x) ;
#define ASSERTEQUALS(expected, actual) ;
#else
#define ASSERT(x) util::Log::assert(__LINE__, __FILE__, x, #x);
#define ASSERTEQUALS(expected, actual) util::Log::assertEquals(__LINE__, __FILE__, expected, actual, #expected, #actual);
#endif // NDEBUG

#endif // !defined(UTIL_LOG_HPP)
