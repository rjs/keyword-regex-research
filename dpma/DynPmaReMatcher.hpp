/*
 * dpma - Research prototype of regular expression matching using a
 * dynamic pattern matching automaton
 *
 * Copyright 2017 Tuukka Haapasalo, Riku Saikkonen, Panu Silvasti and
 * Aalto University
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * The main part of the matching algorithm described in "Experimental
 * Analysis of an Online Dictionary Matching Algorithm for Regular
 * Expressions with Gaps" (SEA 2015).
 */

#ifndef PM_DYNPMAREMATCHER_HPP
#define PM_DYNPMAREMATCHER_HPP

#include "Matcher.hpp"
#include "TransitionTable.hpp"
#include "util-Collections.hpp"
#include "util-BitSet.hpp"
#include "util-Log.hpp"
#include <limits>
#include <map>
#include <set>
#include <list>
#include <string>
#include <vector>
#include <iostream>
#include <utility>

/*
 * Uncomment this define to enable the use of a separate data
 * structure for pending output tuples. By default they are
 * distributed directly to the output tuples, and ignored during
 * matching if they are reached before they are active.
 */
//#define USE_PENDING_OUTPUT

class DynamicPmaReMatcher: public Matcher {
public:
  struct PendingTuple: public virtual Printable {
    size_t _b;
    unsigned int _pattern;
    unsigned int _keyword;
    size_t _e;

    static const size_t no_end = std::numeric_limits<size_t>::max();

    PendingTuple() : PendingTuple(0, 0, 0, 0) { }
    PendingTuple(size_t b_, unsigned int pattern_,
                 unsigned int keyword_, size_t e_) :
      _b(b_), _pattern(pattern_), _keyword(keyword_), _e(e_) { }
    PendingTuple(const PendingTuple & c, size_t add_to_b_e = 0)
      : _b(c._b + add_to_b_e),
        _pattern(c._pattern),
        _keyword(c._keyword),
        _e(c._e == no_end ? c._e : c._e + add_to_b_e) { }
    ~PendingTuple() { }
    size_t b() const { return _b; }
    unsigned int pattern() const { return _pattern; }
    unsigned int keyword() const { return _keyword; }
    size_t e() const { return _e; }
    virtual void print(std::ostream & os) const {
      os << '(' << _b << ','
         << _pattern << ',' << _keyword << ',' << _e << ')';
    }
  };

  using OutputTuple = PendingTuple;

  /*
   * Some alternative choices for data structures for storing output
   * tuples. These alternatives have only been tested with a much
   * simpler early version of the algorithm. The PendingSetType is
   * only used if USE_PENDING_OUTPUT was defined above.
   *
   * Store both pending and current output tuples in std::sets
   * typedef std::set<PendingTuple> PendingSetType;
   * typedef std::set<OutputTuple> OutputSetType;
   * #define OP_INSERT(set, value) (set).insert(value);
   * #define OP_ERASE(set, iterator) (set).erase(iterator++);
   * #define OP_FOREACH(iterator)
   * #define OP_FOREND(iterator) iterator++
   *
   * Store both pending and current output tuples in linked lists
   * typedef std::list<PendingTuple> PendingSetType;
   * typedef std::list<OutputTuple> OutputSetType;
   * #define OP_INSERT(set, value) (set).push_back(value);
   * #define OP_ERASE(set, iterator) iterator = (set).erase(iterator);
   * #define OP_FOREACH(iterator)
   * #define OP_FOREND(iterator) iterator++
   *
   * The uncommented choices below store pending output tuples in an
   * array (std::vector) and current output tuples in a linked list
   * (std::list).
   */
  typedef std::vector<PendingTuple> PendingSetType;
  typedef std::list<OutputTuple> OutputSetType;
#define OP_INSERT(set, value) (set).push_back(value);
#define OP_ERASE(set, iterator) iterator = (set).erase(iterator);
#define OP_FOREACH(iterator)
#define OP_FOREND(iterator) ++iterator

public:
  DynamicPmaReMatcher(std::vector<Pattern> & patterns);
  virtual ~DynamicPmaReMatcher();

  virtual void findMatches(Reader *src_);

private:
#ifdef USE_PENDING_OUTPUT
  void distributeOutput();
#endif
  void distributeOutputTuple(const PendingTuple & po) {
    Matcher::stats.logOperation(Statistics::DistributeOutput);
    if (Matcher::stopOnFirst) {
      Pattern & pat = Matcher::getPattern(po.pattern());
      // If we only find the first matches and this pattern has been
      // found, do not process this tuple
      if (pat.isFound())
        return;
    }
    unsigned int q = kwState[po.pattern()][po.keyword()];
    LOG("Distributing " << po << " to state " << q);
    ASSERT(q < nStates);
    checkAge_state(q);
    OutputSetType & c = currentOutputs[q];

    // Remove some other tuples with the same keyword
    for (OutputSetType::iterator it = c.begin(); it != c.end(); OP_FOREACH(it)) {
      const OutputTuple & o = *it;
      if (o.pattern() == po.pattern() &&
          o.keyword() == po.keyword() &&
          o.b() <= charCount) {
        OP_ERASE(c, it);
      } else {
        OP_FOREND(it);
      }
    }
    OP_INSERT(c, po);
    LOG("State " << q << " is " << c);
  }

  void traverseOutputPath(unsigned int q);
  unsigned int outputFail(unsigned int q);

  void countMaxDistAndKWLen();
  void addKeyword(std::string const & keyword, std::vector<std::map<chartype,
                  unsigned int> > & pgotos, size_t patId, size_t kwId);
  void constructFailStates(
    std::vector<std::map<chartype,unsigned int> > const & pgotos);
  void constructStaticOutputFails();
  unsigned int findStaticOutputFail(unsigned int state);
  void addFailsToGotos(std::vector<std::map<chartype,unsigned int> > & pgotos);

#ifdef USE_PENDING_OUTPUT
  void resetPendingOutput();
#endif
  void initializeOutput();

  void checkAge_state(unsigned int q) {
    if (Matcher::lineMode && stateAge[q] < Matcher::lineCount) {
      stateAge[q] = Matcher::lineCount;
      currentOutputs[q].clear();
      for (const OutputTuple & o : initial_currentOutputs[q])
        OP_INSERT(currentOutputs[q], OutputTuple(o, bolPos));
    }
  }

  void checkAge_pat(size_t patId) {
    if (Matcher::lineMode && patAge[patId] < Matcher::lineCount) {
      patAge[patId] = Matcher::lineCount;
      maxKWFound[patId] = -1;
      needFallback[patId] = Matcher::getPattern(patId).isAlwaysFallback();
    }
  }

private:

  unsigned int maxDist;
  unsigned int maxKWLen;
  unsigned int newState;
  size_t nStates;
  util::BitSet keywordState;
  TransitionTable<chartype, unsigned int> *gotos;
  std::set<chartype> usedChars;
  unsigned int *fails;
  unsigned int *outputFails;
#ifdef USE_PENDING_OUTPUT
  PendingSetType *pendingOutput;
#endif
  OutputSetType *currentOutputs;
  OutputSetType *initial_currentOutputs;
  int *maxKWFound;
  bool *needFallback;
  unsigned int **kwState;
  size_t *stateAge; // lineCount for currentOutputs
  size_t *patAge; // lineCount for needFallback, maxKWFound
};

class DynamicPmaReMatcherFactory: public MatcherFactory {
public:
  DynamicPmaReMatcherFactory() { }
  Matcher * createMatcher(std::vector<Pattern> & patterns) {
    return new DynamicPmaReMatcher(patterns);
  }
};

#endif // PM_DYNPMAREMATCHER_HPP
