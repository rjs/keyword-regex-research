# spamassassin-extract.pl - extract regular expressions from
# SpamAssassin rules files
#
# Copyright 2016 Riku Saikkonen and Aalto University
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# Usage: cat *.cf | perl spamassassin-extract.pl [-bkwp] >sa-pats
#   -b: leave out patterns with backreferences
#   -k: leave out patterns with no keywords
#   -w: convert \< \> to \b (for RE2, which doesn't support \< or \>)
#   -p: debug option to print ignored patterns with "XXX \d ignored:" lines
#
# Also always converts the output from Perl syntax to POSIX extended
# regular expression syntax, leaving out expressions that require
# features that are not present in the POSIX syntax.
#
# Some pattern counts and special cases in comments below are from
# Mail-SpamAssassin-rules-3.4.0.r1565117.tgz (S)
# svn://svn.debian.org/svn/pkg-listmaster@450 (D)
#
# These SpamAssassin rule syntaxes are ignored:
#   exists:...
#   eval:...
#   !~ (only 3 instances in (S))
#   /.../ options other than ims (at least /x would need more parsing)
#   some m{...}-like syntaxes (m,..., is recognised)
# These parts of rules are ignored (the rules are kept):
#   /.../ims options (see comment on $mtype below)
# And some patterns are changed as detailed in comments below.
#
# The intention is to create a set of patterns that can be used to
# match against a continuous stream of text. Thus, there is no
# separation between matching either message headers or body text as
# in the SpamAssassin rules. In addition, a SpamAssassin rule like
# "header X Subject =~ /something/" is converted to
# "^Subject:\s+.*something" (while SpamAssassin would match
# /something/ only against the content of the Subject header), with
# some handling of an initial ^ in /something/.
#
# FIXME Other missing features:
#  - Backreference numbers in the produced patterns can be incorrect,
#    because a) non-capturing (?:...) is converted to (...) and b) the
#    numbers seem to be defined differently in Perl and egrep when
#    there are nested () expressions.
#  - Do something with "uri" rules.
#  - "full" rules could be treated like "body" (but there are very few
#    of these that use regular expressions).
#  - A non-initial ^ is not converted properly in "header X Y =~"-type
#    rules.
#  - Also see the comments below.

use warnings;
use strict;
use Getopt::Long;

my $no_backreferences = 0;
my $must_have_keywords = 0;
my $word_boundaries_b = 0;
my $print_ignored = 0;
GetOptions('no-backreferences|b' => \$no_backreferences,
           'must-have-keywords|k' => \$must_have_keywords,
           'word-boundaries-b|w' => \$word_boundaries_b,
           'print-ignored|p' => \$print_ignored);

sub pcretoegrep ($$) {
    my ($pat, $hasextrakws) = @_;

    # Remove some special cases
    # egrep: Invalid back reference (S)
    # ^Subject:[	 ].*\b([a-z]([-_. =~/:,*!@#$%^&+;"'<>\\])\1{0,2}){4}
    return '' if ($pat =~ /\\1\{0,2\}\)\{4\}$/);
    # Too strange with an unescaped ] (3 patterns in (D))
    return '' if ($pat =~ /\(\?:w\|\\\\\\\/\\\\\\\/\|VV\|\[\\xC5\]/);

    # Some "bugfixes" or syntax changes to a few individual patterns
    # Remove useless \b? (1 pattern in (S))
    $pat =~ s/\\b\?//;
    # {1.3} to {1,3} (3 patterns in (D))
    $pat =~ s/\{1\.3\}/{1,3}/g;
    # in G(?:\]\w\[)?R(?:]\w\[)?A change ] to \] (D)
    $pat =~ s/\\\[\)\?R\(\?:\]\\w/\\[)?R(?:\\]\\w/;
    # in /^\[\ [a-z0-9]{16}]\ / change ] to \] (D)
    $pat =~ s/\[a-z0-9\]\{16\}\]\\ /\[a-z0-9\]\{16\}\\]\\ /;
    # in ?n.?e|\[xz].?a.?n remove \ (D)
    $pat =~ s/\?n\.\?e\|\\\[xz\]\.\?a\.\?n/?n.?e\|\[xz\].?a.?n/;
    # in \.\w*price\s*{ escape last { (D)
    $pat =~ s/^\\\.\\w\*price\\s\*\{$/\\.\\w*price\\s*\\{/;
    # \[ck]redit to [ck]redit
    $pat =~ s/^\\\[ck\]redit/[ck]redit/;

    # Uncommenting these would leave out some patterns from (S) that
    # match very often (>10000 matched lines in public corpus)
    #
    # return '' if ($pat eq '^> ');
    # return '' if ($pat eq '\w@\S+\.\w');
    # return '' if ($pat =~ /^\^MESSAGEID:\[	 \]\*\.\{1,15\}.|<\.\{0,4\}\\.$/);

    if ($must_have_keywords && !$hasextrakws) {
        # Patterns without any keywords (S)
        return '' if ($pat eq '\b(?:[a-z]{5,}[\s\.]+){10}');
        return '' if ($pat eq '^[a-z0-9]{6,24}[-_a-z0-9]{12,36}[a-z0-9]{6,24}\s*\z');
        return '' if ($pat eq '\b(?:[a-z]{8,}[\s\.]+){6}');
        return '' if ($pat eq '\b(?:[a-z]{6,}[\s\.]+){9}');
        return '' if ($pat eq '\b(?:[a-z]{5,}[\s\.]+){10}');
        return '' if ($pat eq '\S');
        return '' if ($pat eq '^\s*\S');
        return '' if ($pat eq '[a-z0-9]{6}\s{8}[a-z0-9]{5}');
        return '' if ($pat eq '(?:[\x80-\xff].?){4}');
    }

    if ($no_backreferences) {
        return '' if ($pat =~ /\\\d/);
    }

    # remove patterns that use look-ahead e.g. (?=foo) (102 patterns (S))
    return '' if ($pat =~ /\(\?[=!<]/);

    # remove patterns that use independent subexpressions (?>foo) (only 1 (S))
    return '' if ($pat =~ /\(\?>/);

    # remove patterns that use newlines (\n) (only 12 (S))
    # FIXME or convert to . or ' ' or something?
    return '' if ($pat =~ /\\n/);

    # remove \x00-style escapes that produce NUL or a newline
    return '' if ($pat =~ /\\x00/);    # (only 1)
    return '' if ($pat =~ /\\x0[aA]/); # (none)

    # remove \000-style escapes (only 1 pattern (S) and it has no keywords)
    return '' if ($pat =~ /\\[012][0-9][0-9]/);

    # remove patterns that use possessive matching *+ etc. (none (S))
    return '' if ($pat =~ /[*+?}]\+/);

    # convert \x00-style escapes to corresponding chars
    $pat =~ s/\\x([0-9a-fA-F][0-9a-fA-F])/chr(hex($1))/eg;
    # remove unconverted \x syntaxes like \x{...} (none (S))
    return '' if ($pat =~ /\\x/);

    # convert non-greedy matching *? etc. to normal matching
    $pat =~ s/([*+?}])\?/$1/g;

    # \e \r \t to appropriate characters
    $pat =~ s/\\e/\e/g;
    $pat =~ s/\\r/\r/g;
    $pat =~ s/\\t/\t/g;
    # \' to ' (D)
    $pat =~ s/\\'/'/g;

    # convert \z and \Z to $ (this is approximately correct)
    $pat =~ s/\\[zZ]/\$/g;

    if ($word_boundaries_b) {
        # convert \< and \> to \b
        $pat =~ s/\\[<>]/\\b/g;
    }

    # convert (?:...) non-capturing match to (...)
    $pat =~ s/\(\?[a-z^-]*\:/(/g;

    # remove \ escapes in character classes
    $pat =~ s/(^|[^\\])\[\\\^\]/$1\\^/g;            # [\^] => \^
    $pat =~ s/(^|[^\\])(\[\^?)([^]]*[^]\\]|)\\]/$1$2]$3/g; # \] to initial ]
    $pat =~ s/(^|[^\\])\[\\\^([^]\\]|\\.)/$1\[$2^/g; # [\^...]
    $pat =~ s/(^|[^\\])(\[\^?\]?[^]]*)\\-([^]]*)\]/$1$2$3-]/g; # \-
    # remove other char class escapes except \\
    while ($pat =~ s/(^|[^\\])(\[\^?\]?(?:[^]\\]|\\.)*)\\([^\\])/$1$2$3/g) {}
    # move - to last in char classes: [a-zA-Z0-9-_] to [a-zA-Z0-9_-]
    $pat =~ s/(^|[^\\])(\[[^]]*[^]-]-[^]-])-([^]]*)\]/$1$2$3-]/g;

    # convert \[dDsS] to corresponding character classes
    # (POSIX supports \w and \W)
    # FIXME support these inside char classes, e.g. [\s\.]
    $pat =~ s/\\d/[0-9]/g;
    $pat =~ s/\\D/[^0-9]/g;
    # (for \s \S, the commented lines would be the correct replacement, but
    # \n doesn't work and the simpler one is good enough)
    # $pat =~ s/\\s/[\t\n\f\r ]/g;
    # $pat =~ s/\\S/[^\t\n\f\r ]/g;
    $pat =~ s/\\s/[\t ]/g;
    $pat =~ s/\\S/[^\t ]/g;

    die "unknown use of (?...)" if ($pat =~ /(^|[^\\])\(\?/);
    die "unknown use of \\x escape" if ($pat =~ /\\[A-VXYZac-vxyz]/);
    # \w \W \< \> \b, and \1 \2 etc. (backreferences) are kept (egrep supports)

    return $pat;
}

while (<>) {
    my ($hdr, $ext, $pat, $mtype);

    next if /^#/;

    s/[ \r\n]*$//;

    if (s/^[ \t]*(mime)?header[ \t]*[^ \t]*[ \t]+//) {
        if (($hdr, $ext, $pat, $mtype) =
            ($_ =~ /^([^ \t:]+)(:[^ \t]*)?[ \t]+=~[ \t]+\/(.*)\/([ims]*)[ #"]*$/)) {
            ;
        } elsif (($hdr, $ext, $pat, $mtype) =
                 ($_ =~ /^([^ \t:]+)(:[^ \t]*)?[ \t]+=~[ \t]+m,(.*),([ims]*)[ #"]*$/)) {
            ;
        } elsif (($hdr) = ($_ =~ /exists:([^ \t]+)[ \t]*$/)) {
            $pat = "^$hdr:";
            $hdr = 'ALL';
            $mtype = 'i';
        } else {
            print "XXX 1 ignored: $_\n" if $print_ignored;
            next;
        }
        $ext = '' if !defined($ext);
    } elsif (s/^[ \t]*(raw)?(body|full)[ \t]*[^ \t]*[ \t]+//) {
        unless (($pat, $mtype) = ($_ =~ /^\/(.*)\/([ims]*)$/)) {
            print "XXX 2 ignored: $_\n" if $print_ignored;
            next;
        }
        $hdr = 'ALL';
        $ext = '';
    } else {
        if ($print_ignored and (/\<header/ or /body\>/)) {
            print "XXX 3 ignored: $_\n";
        }
        next;
    }

    # print "$hdr;$ext;$pat;$mtype\n";

    $pat = pcretoegrep($pat, ($hdr ne 'ALL'));
    if ($pat eq '') {
        print "XXX 4 ignored: $_\n" if $print_ignored;
        next;
    }

    # This $mtype has the Perl matching flags (e.g. i=case-
    # insensitive, m=multiline), but it is ignored by the code below,
    # because POSIX regular expression syntax does not have this
    # concept. Perhaps the output format of this script could be
    # extended to include some of the flags, because most
    # single-pattern matchers do support them (e.g. REG_ICASE in
    # regexec(3) in the C library). But at least grep -Ef would not
    # work.
    $mtype = join('', sort(split(//, $mtype)));

    if ($hdr eq 'ALL') {
        print "$pat\n";
    } elsif ($pat =~ /^\^/) { # initial ^
        $pat =~ s/^\^//;
        print "^$hdr:[\t ]*$pat\n";
    } else {
        if ($hdr =~ /^ToCc$/i) {
            $hdr = '(To|Cc)';
        } elsif ($hdr =~ /^EnvelopeFrom$/i) {
            $hdr = 'Envelope-From'; # for lack of anything better
        } elsif ($hdr eq 'MESSAGEID') {
            $hdr = '(Resent-|X-|)Message-Id';
        }

        # remove some instances of non-initial ^
        $pat =~ s/\(\^/(/g;
        $pat =~ s/\|\^/|/g;
        print "^$hdr:[\t ].*$pat\n";
    }
}

exit(0);
