# randomize-keywords.pl - create more patterns from an existing set by
# modifying keywords randomly
#
# Copyright 2016 Riku Saikkonen and Aalto University
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

# Usage: perl randomize-keywords.pl [-k K] [-n PATCOUNT] [-s SEED] [-x] [FILE ...]
#   -k K: randomly change at most K keywords per pattern
#   -n PATCOUNT: create PATCOUNT patterns from each input line
#   -s SEED: set random seed
#   -x: also include the original unmodified patterns in the output
# Defaults: -k 1 -n 10 -s 1 and -x unset.
#
# The script takes a list of regular expressions (one per line) as
# input, and randomly modifies one (usually non-initial) character in
# K randomly chosen 5-character or longer keywords of each input
# expression. This is repeated PATCOUNT times, so the output should
# have at most PATCOUNT times the number of input lines (less if some
# of the input patterns do not have long keywords or if this simple
# "parser" does not find them). The generated patterns thus have the
# same structure as the input patterns, but slightly different text.
#
# It is usually useful to pipe the output of this script though | sort
# | uniq to filter out any duplicates caused by the randomization.
# Consider using the -x option to include the original patterns as
# well.
#
# This has been tested only with input patterns in POSIX extended
# syntax, though the script is generic enough that other regular
# expression syntaxes should work as well.

use warnings;
use strict;
use Getopt::Std;
use List::Util qw(shuffle);

sub ran_kw ($$) {
    my ($pat, $amount) = @_;
    # Avoid parsing by considering as a keyword any string of 5 or
    # more word characters
    my @kws = split(/(\w{5,})/, $pat);
    return $pat if ($#kws < 1); # can't modify

    my $numkws = int(($#kws + 1) / 2);
    my @alterkws = shuffle(0..$numkws-1);
    @alterkws = @alterkws[0..$amount-1] if ($amount < $numkws);
    @alterkws = map { 1 + 2 * $_ } @alterkws;

    foreach my $alterkw (@alterkws) {
        # Modify one randomly selected character in the randomly selected
        # keyword; except not the first char (in case of \bfoo or similar)
        my $alterchar = 1 + int(rand(length($kws[$alterkw]) - 1));
        # (don't use _ in replacements)
        my $newchar = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
                             rand(62),
                             1);
        substr($kws[$alterkw], $alterchar, 1) = $newchar;
    }

    return join('', @kws);
}

our $opt_k = 1;
our $opt_n = 10;
our $opt_s = 1;
our $opt_x = 0;
getopts('k:n:s:x') or die "getopts";

srand($opt_s);

while (<>) {
    chop;
    my $pat = $_;
    print $pat . "\n" if ($opt_x);
    for (my $i = 1; $i <= $opt_n; $i++) {
        print ran_kw($pat, $opt_k) . "\n";
    }
}

exit 0;
